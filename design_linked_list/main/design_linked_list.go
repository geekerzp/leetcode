package main

import (
	"github.com/davecgh/go-spew/spew"
)

type Node struct {
	Val  int
	Next *Node
}

type MyLinkedList struct {
	Head *Node
	Tail *Node
	Size int
}

/** Initialize your data structure here. */
func Constructor() MyLinkedList {
	return MyLinkedList{}
}

/** Get the value of the index-th node in the linked list. If the index is invalid, return -1. */
func (this *MyLinkedList) Get(index int) int {
	if index < 0 || index >= this.Size {
		return -1
	}
	node := this.Head
	for i := 0; i < index; i++ {
		node = node.Next
	}
	return node.Val
}

/** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
func (this *MyLinkedList) AddAtHead(val int) {
	this.Head = &Node{Val: val, Next: this.Head}
	if this.Size == 0 {
		this.Tail = this.Head
	}
	this.Size++
}

/** Append a node of value val to the last element of the linked list. */
func (this *MyLinkedList) AddAtTail(val int) {
	node := &Node{Val: val}
	if this.Size == 0 {
		this.Head = node
		this.Tail = this.Head
	} else {
		this.Tail.Next = node
		this.Tail = node
	}
	this.Size++
}

/** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
func (this *MyLinkedList) AddAtIndex(index int, val int) {
	if index < 0 || index > this.Size {
		return
	}
	if index == 0 {
		this.AddAtHead(val)
		return
	}
	if index == this.Size {
		this.AddAtTail(val)
		return
	}
	dummy := &Node{Next: this.Head}
	prev := dummy
	for i := 0; i < index; i++ {
		prev = prev.Next
	}
	prev.Next = &Node{Val: val, Next: prev.Next}
	this.Size++
}

/** Delete the index-th node in the linked list, if the index is valid. */
func (this *MyLinkedList) DeleteAtIndex(index int) {
	if index < 0 || index >= this.Size {
		return
	}
	dummy := Node{Next: this.Head}
	prev := &dummy
	for i := 0; i < index; i++ {
		prev = prev.Next
	}
	prev.Next = prev.Next.Next
	if index == 0 {
		this.Head = prev.Next
	}
	if index == this.Size-1 {
		this.Tail = prev
	}
	this.Size--
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Get(index);
 * obj.AddAtHead(val);
 * obj.AddAtTail(val);
 * obj.AddAtIndex(index,val);
 * obj.DeleteAtIndex(index);
 */

func main() {
	obj := Constructor()
	obj.AddAtHead(7)

	obj.AddAtHead(2)

	obj.AddAtHead(1)

	obj.AddAtIndex(3, 0)

	obj.DeleteAtIndex(2)

	obj.AddAtHead(6)

	obj.AddAtTail(4)

	obj.Get(4)
	obj.AddAtHead(4)

	obj.AddAtIndex(5, 0)

	obj.AddAtHead(6)
	spew.Dump(obj)
}
