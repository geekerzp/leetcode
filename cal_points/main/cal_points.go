package main

import "strconv"

func calPoints(ops []string) int {
	var stack []int
	for _, op := range ops {
		if op == "+" {
			stack = append(stack, stack[len(stack)-1]+stack[len(stack)-2])
		} else if op == "C" {
			stack = stack[:len(stack)-1]
		} else if op == "D" {
			stack = append(stack, 2 * stack[len(stack)-1])
		} else {
			val, _ := strconv.Atoi(op)
			stack = append(stack, val)
		}
	}

	var sum int
	for _, val := range stack {
		sum += val
	}

	return sum
}
