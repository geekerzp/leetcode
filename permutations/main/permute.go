package main

import "github.com/davecgh/go-spew/spew"

func contains(intSlice []int, searchInt int) bool {
	for _, value := range intSlice {
		if value == searchInt {
			return true
		}
	}
	return false
}

func backtrack(list *[][]int, tempList *[]int, nums []int) {
	if len(*tempList) == len(nums) {
		*list = append(*list, append([]int{}, *tempList...))
	} else {
		for _, num := range nums {
			if contains(*tempList, num) {
				continue
			}
			*tempList = append(*tempList, num)
			backtrack(list, tempList, nums)
			*tempList = (*tempList)[:len(*tempList)-1]
		}
	}
}

func permute(nums []int) [][]int {
	var res [][]int
	backtrack(&res, &[]int{}, nums)
	return res
}

func main() {
	spew.Dump(permute([]int{1, 2, 3}))
}
