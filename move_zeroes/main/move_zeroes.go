package main

func moveZeros(nums []int) {
	// Count the zeros.
	numZeros := 0
	for _, num := range nums {
		if num == 0 {
			numZeros++
		}
	}

	// Make all the non-zero elements retain their orignal order.
	var ans []int
	for _, num := range nums {
		if num != 0 {
			ans = append(ans, num)
		}
	}

	// Move all zeros to the end.
	for i := numZeros; i > 0; i-- {
		ans = append(ans, 0)
	}

	// Combine the result.
	for i := 0; i < len(nums); i++ {
		nums[i] = ans[i]
	}
}
