package main

func longestIncreasingPath(matrix [][]int) int {
	if len(matrix) == 0 {
		return 0
	}

	m, n := len(matrix), len(matrix[0])

	cache := make([][]int, m)
	for i := 0; i < m; i++ {
		cache[i] = make([]int, n)
	}

	dirs := [][]int{{0, 1}, {1, 0}, {0, -1}, {-1, 0}}

	max := 1
	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			max = maxInt(max, dfs(matrix, dirs, i, j, m, n, cache))
		}
	}

	return max
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func dfs(matrix [][]int, dirs [][]int, i int, j int, m int, n int, cache [][]int) int {
	if cache[i][j] != 0 {
		return cache[i][j]
	}

	max := 1
	for _, dir := range dirs {
		x, y := i+dir[0], j+dir[1]
		if x < 0 || x >= m || y < 0 || y >= n || matrix[x][y] <= matrix[i][j] {
			continue
		}
		max = maxInt(max, 1+dfs(matrix, dirs, x, y, m, n, cache))
	}

	cache[i][j] = max
	return max
}
