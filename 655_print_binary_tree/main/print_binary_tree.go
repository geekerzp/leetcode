package main

import (
	"strconv"

	"github.com/davecgh/go-spew/spew"
)

/**
 * https://www.youtube.com/watch?v=ipIL1qVAazk&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=53&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func printTree(root *TreeNode) [][]string {
	height := getHeight(root)
	weight := (1 << uint(height)) - 1
	ans := make([][]string, height)
	for i := range ans {
		ans[i] = make([]string, weight)
	}
	fill(root, &ans, 0, 0, weight-1)
	return ans
}

func fill(root *TreeNode, ans *[][]string, h int, l int, r int) {
	if root == nil {
		return
	}
	mid := (l + r) / 2
	(*ans)[h][mid] = strconv.Itoa(root.Val)
	fill(root.Left, ans, h+1, l, mid-1)
	fill(root.Right, ans, h+1, mid+1, r)
}

func getHeight(root *TreeNode) int {
	if root == nil {
		return 0
	}
	return max(getHeight(root.Left), getHeight(root.Right)) + 1
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {
	root := &TreeNode{Val: 1}
	root.Left = &TreeNode{Val: 2}
	spew.Dump(printTree(root))
}
