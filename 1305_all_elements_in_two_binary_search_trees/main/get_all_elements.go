package main

import "github.com/davecgh/go-spew/spew"

/**
 * https://leetcode.com/problems/all-elements-in-two-binary-search-trees/
 * https://www.youtube.com/watch?v=2cbsWlAHlj4&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=2&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func getAllElements(root1 *TreeNode, root2 *TreeNode) []int {
	var elements1 []int
	var elements2 []int
	dfs(root1, &elements1)
	dfs(root2, &elements2)

	var ans []int
	var i, j int
	for i < len(elements1) || j < len(elements2) {
		if i == len(elements1) {
			ans = append(ans, elements2[j:]...)
			break
		}
		if j == len(elements2) {
			ans = append(ans, elements1[i:]...)
			break
		}

		if elements1[i] <= elements2[j] {
			ans = append(ans, elements1[i])
			i++
		} else {
			ans = append(ans, elements2[j])
			j++
		}
	}

	return ans
}

func dfs(root *TreeNode, elements *[]int) {
	if root == nil {
		return
	}
	dfs(root.Left, elements)
	*elements = append(*elements, root.Val)
	dfs(root.Right, elements)
}

func main() {
	root1 := &TreeNode{Val: 2}
	root1.Left = &TreeNode{Val: 1}
	root1.Right = &TreeNode{Val: 4}

	root2 := &TreeNode{Val: 1}
	root2.Left = &TreeNode{Val: 0}
	root2.Right = &TreeNode{Val: 3}

	ans := getAllElements(root1, root2)
	spew.Dump(ans)
}
