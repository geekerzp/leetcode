package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isValidBST(root *TreeNode) bool {
	if root == nil {
		return true
	}

	var stack []*TreeNode
	var pre *TreeNode

	for root != nil || len(stack) != 0 {
		for root != nil {
			stack = append([]*TreeNode{root}, stack...)
			root = root.Left
		}
		root, stack = stack[0], stack[1:len(stack)]
		if pre != nil && root.Val <= pre.Val {
			return false
		}
		pre = root
		root = root.Right
	}

	return true
}
