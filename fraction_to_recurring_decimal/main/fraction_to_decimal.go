package main

import (
	"strconv"
	"strings"
)

func fractionToDecimal(numerator int, denominator int) string {
	if numerator == 0 {
		return "0"
	}
	var res []string

	// "+" or "-"
	if numerator*denominator < 0 {
		res = append(res, "-")
	}
	num := abs(numerator)
	den := abs(denominator)

	// integral part
	res = append(res, strconv.Itoa(num/den))
	num %= den
	if num == 0 {
		return strings.Join(res, "")
	}

	// fractional part
	res = append(res, ".")
	numMap := map[int]int{num: len(res)}
	for num != 0 {
		num *= 10
		res = append(res, strconv.Itoa(num/den))
		num %= den
		if _, exists := numMap[num]; exists {
			i := numMap[num]
			res = append(res[:i], append([]string{"("}, res[i:]...)...)
			res = append(res, ")")
			break
		} else {
			numMap[num] = len(res)
		}
	}

	return strings.Join(res, "")
}

func abs(n int) int {
	y := n >> 63
	return (n ^ y) - y
}
