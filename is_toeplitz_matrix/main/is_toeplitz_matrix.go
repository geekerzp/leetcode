package main

func isToeplitzMatrix(matrix [][]int) bool {
	groups := make(map[int]int)

	for r := 0; r < len(matrix); r++ {
		for c := 0; c < len(matrix[0]); c++ {
			if _, exists := groups[r-c]; !exists {
				groups[r-c] = matrix[r][c]
			} else if groups[r-c] != matrix[r][c] {
				return false
			}
		}
	}

	return true
}
