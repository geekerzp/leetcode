package main

import (
	"math"

	"github.com/davecgh/go-spew/spew"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func distributeCoins(root *TreeNode) int {
	var ans int
	balance(root, &ans)
	return ans
}

func balance(root *TreeNode, ans *int) int {
	if root == nil {
		return 0
	}

	l := balance(root.Left, ans)
	r := balance(root.Right, ans)
	(*ans) += abs(l) + abs(r)

	return root.Val - 1 + l + r
}

func abs(i int) int {
	return int(math.Abs(float64(i)))
}

func main() {
	t := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val:   0,
			Right: &TreeNode{Val: 3}},
		Right: &TreeNode{Val: 0}}
	ans := distributeCoins(t)
	spew.Dump(ans)
}
