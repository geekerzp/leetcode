package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func binaryTreePaths(root *TreeNode) []string {
	var ans []string
	if root != nil {
		searchBT(root, "", &ans)
	}
	return ans
}

func searchBT(root *TreeNode, path string, ans *[]string) {
	if root.Left == nil && root.Right == nil {
		*ans = append(*ans, fmt.Sprintf("%s%d", path, root.Val))
	}
	if root.Left != nil {
		searchBT(root.Left, fmt.Sprintf("%s%d->", path, root.Val), ans)
	}
	if root.Right != nil {
		searchBT(root.Right, fmt.Sprintf("%s%d->", path, root.Val), ans)
	}
}

func main() {

}
