package main

import (
	"math"
)

func numSquares(n int) int {
	dp := make([]int, n+1)
	dp[0] = 0

	for i := 1; i < len(dp); i++ {
		dp[i] = math.MaxInt64
	}

	for i := 0; i <= n; i++ {
		for j := 1; i+j*j <= n; j++ {
			if dp[i]+1 < dp[i+j*j] {
				dp[i+j*j] = dp[i] + 1
			}
		}
	}

	return dp[len(dp)-1]
}

func main() {
	println(numSquares(13))
}
