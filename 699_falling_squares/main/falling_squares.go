package main

/**
 * https://leetcode.com/problems/falling-squares/
 */

func fallingSquares(positions [][]int) []int {
	qans := make([]int, len(positions))
	for i := 0; i < len(positions); i++ {
		left := positions[i][0]
		size := positions[i][1]
		right := left + size
		qans[i] += size

		for j := i + 1; j < len(positions); j++ {
			left2 := positions[j][0]
			size2 := positions[j][1]
			right2 := left2 + size2
			if left2 < right && left < right2 {
				qans[j] = maxInt(qans[j], qans[i])
			}
		}
	}

	var ans []int
	cur := -1
	for _, x := range qans {
		cur = maxInt(cur, x)
		ans = append(ans, cur)
	}
	return ans
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {
	fallingSquares([][]int{{1, 2}, {2, 3}, {6, 1}})
}
