package main

import (
	"math"

	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/split-array-into-fibonacci-sequence/
 * https://www.youtube.com/watch?v=evQCfq5wpns&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=2&t=0s
 */

func splitIntoFibonacci(S string) []int {
	var nums []int
	dfs(0, S, &nums)
	return nums
}

func dfs(pos int, S string, nums *[]int) bool {
	n := len(S)
	if pos == n {
		return len(*nums) >= 3
	}

	var maxLen int
	if S[pos] == '0' {
		maxLen = 1
	} else {
		maxLen = 10
	}

	var num int
	for i := pos; i < minInt(pos+maxLen, n); i++ {
		num = num*10 + int(S[i]-'0')
		if len(*nums) >= 2 {
			sum := (*nums)[len(*nums)-1] + (*nums)[len(*nums)-2]
			if num > math.MaxInt32 {
				break
			}
			if num > sum {
				break
			} else if num < sum {
				continue
			}
			// Num must equals to sum
		}
		*nums = append(*nums, num)
		if dfs(i+1, S, nums) {
			return true
		}
		*nums = (*nums)[:len(*nums)-1]
	}

	return false
}

func minInt(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func main() {
	spew.Dump(splitIntoFibonacci("0123"))
}
