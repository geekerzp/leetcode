package main

import (
	"math/rand"
	)

type Solution struct {
	Array    []int
	Original []int
}

func Constructor(nums []int) Solution {
	return Solution{Array: nums, Original: append([]int{}, nums...)}
}

func randRange(min int, max int) int {
	return rand.Intn(max-min) + min
}

func swapAt(array []int, i int, j int) {
	temp := array[i]
	array[i] = array[j]
	array[j] = temp
}

/** Resets the array to its original configuration and return it. */
func (this *Solution) Reset() []int {
	this.Array = this.Original
	this.Original = append([]int{}, this.Original...)
	return this.Original
}

/** Returns a random shuffling of the array. */
func (this *Solution) Shuffle() []int {
	for i := 0; i < len(this.Array); i++ {
		swapAt(this.Array, i, randRange(i, len(this.Array)))
	}
	return this.Array
}

/**
 * Your Solution object will be instantiated and called as such:
 * obj := Constructor(nums);
 * param_1 := obj.Reset();
 * param_2 := obj.Shuffle();
 */
