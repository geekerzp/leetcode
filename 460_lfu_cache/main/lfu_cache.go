package main

import "container/list"

type CacheNode struct {
	Key     int
	Value   int
	Freq    int
	KeyNode *list.Element
}

type LFUCache struct {
	NodeMap     map[int]*CacheNode
	FreqListMap map[int]*list.List
	Capacity    int
	MinFreq     int
}

func Constructor(capacity int) LFUCache {
	return LFUCache{
		Capacity:    capacity,
		MinFreq:     0,
		NodeMap:     make(map[int]*CacheNode),
		FreqListMap: make(map[int]*list.List),
	}
}

func (c *LFUCache) Get(key int) int {
	cacheNode, ok := c.NodeMap[key]
	if !ok {
		return -1
	}
	c.touch(cacheNode)
	return cacheNode.Value
}

func (c *LFUCache) Put(key int, value int) {
	if c.Capacity == 0 {
		return
	}

	cacheNode, ok := c.NodeMap[key]
	if ok {
		cacheNode.Value = value
		c.touch(cacheNode)
		return
	}

	if len(c.NodeMap) == c.Capacity {
		keyToEvict := c.FreqListMap[c.MinFreq].Back()
		c.FreqListMap[c.MinFreq].Remove(keyToEvict)
		delete(c.NodeMap, keyToEvict.Value.(int))
	}

	freq := 1
	c.MinFreq = freq

	if _, ok := c.FreqListMap[freq]; !ok {
		c.FreqListMap[freq] = list.New()
	}

	c.FreqListMap[freq].PushFront(key)
	c.NodeMap[key] = &CacheNode{
		Key:     key,
		Value:   value,
		Freq:    freq,
		KeyNode: c.FreqListMap[freq].Front(),
	}
}

func (c *LFUCache) touch(cacheNode *CacheNode) {
	// Step 1: update the frequency
	prevFreq := cacheNode.Freq
	cacheNode.Freq++
	freq := cacheNode.Freq

	// Step 2: remove the entry from old freq list
	c.FreqListMap[prevFreq].Remove(cacheNode.KeyNode)
	if c.FreqListMap[prevFreq].Len() == 0 && prevFreq == c.MinFreq {
		// Delete the list
		delete(c.FreqListMap, prevFreq)

		// Increase the min freq
		c.MinFreq++
	}

	// Step 3: insert the key into the front of the new freq list
	if _, ok := c.FreqListMap[freq]; !ok {
		c.FreqListMap[freq] = list.New()
	}
	c.FreqListMap[freq].PushFront(cacheNode.Key)

	// Step 4: update the pointer
	cacheNode.KeyNode = c.FreqListMap[freq].Front()
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
