package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func numComponents(head *ListNode, G []int) int {
	f := make(map[int]struct{})
	for _, v := range G {
		f[v] = struct{}{}
	}

	g := make(map[int][]int)
	u := head.Val
	for head.Next != nil {
		head = head.Next
		v := head.Val

		_, ok1 := f[u]
		_, ok2 := f[v]
		if ok1 && ok2 {
			g[u] = append(g[u], v)
			g[v] = append(g[v], u)
		}
		u = v
	}

	var ans int
	visited := make(map[int]struct{})
	for _, u := range G {
		if _, ok := visited[u]; !ok {
			ans++
			dfs(u, g, &visited)
		}
	}
	return ans
}

func dfs(cur int, g map[int][]int, visited *map[int]struct{}) {
	(*visited)[cur] = struct{}{}
	for _, next := range g[cur] {
		if _, ok := (*visited)[next]; !ok {
			dfs(next, g, visited)
		}
	}
}
