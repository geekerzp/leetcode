package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/combination-sum-iii/
 * https://www.youtube.com/watch?v=UwdX19UvoCI&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=30
 */
func combinationSum3(k int, n int) [][]int {
	var curr []int
	var ans [][]int
	dfs(1, k, n, &curr, &ans)
	return ans
}

func dfs(s int, k int, n int, curr *[]int, ans *[][]int) {
	if len(*curr) == k {
		if sum(*curr) == n {
			cand := make([]int, len(*curr))
			copy(cand, *curr)
			*ans = append(*ans, cand)
		}
		return
	}

	for i := s; i <= 9; i++ {
		*curr = append(*curr, i)
		dfs(i+1, k, n, curr, ans)
		*curr = (*curr)[:len(*curr)-1]
	}
}

func sum(arr []int) int {
	var sum int
	for _, v := range arr {
		sum += v
	}
	return sum
}

func main() {
	ans := combinationSum3(3, 9)
	spew.Dump(ans)
}
