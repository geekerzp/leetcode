package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/remove-invalid-parentheses/
 * https://www.youtube.com/watch?v=2k_rS_u6EBk&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=27
 */

func removeInvalidParentheses(s string) []string {
	var l, r int

	for _, ch := range s {
		if ch == '(' {
			l++
		} else if ch == ')' {
			if l == 0 {
				r++
			} else {
				l--
			}
		}
	}

	var ans []string
	dfs(s, 0, l, r, &ans)
	return ans
}

func dfs(s string, start int, l int, r int, ans *[]string) {
	if l == 0 && r == 0 {
		if isValid(s) {
			*ans = append(*ans, s)
		}
		return
	}

	for i := start; i < len(s); i++ {
		if i != start && s[i] == s[i-1] {
			continue
		}
		if s[i] == ')' && r > 0 {
			b := []byte(s)
			b = append(b[:i], b[i+1:]...)
			curr := string(b)
			dfs(curr, i, l, r-1, ans)
		} else if s[i] == '(' && l > 0 {
			b := []byte(s)
			b = append(b[:i], b[i+1:]...)
			curr := string(b)
			dfs(curr, i, l-1, r, ans)
		}
	}
}

func isValid(s string) bool {
	var count int
	for _, ch := range s {
		if ch == '(' {
			count++
		}
		if ch == ')' {
			count--
		}
		if count < 0 {
			return false
		}
	}
	return count == 0
}

func main() {
	ans := removeInvalidParentheses("()())()")
	spew.Dump(ans)
}
