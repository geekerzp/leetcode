package main

func canCompleteCircuit(gas []int, cost []int) int {
	var sumGas, sumCost, start, tank int

	for i := 0; i < len(gas); i++ {
		sumGas += gas[i]
		sumCost += cost[i]
		tank += gas[i] - cost[i]

		if tank < 0 {
			start = i + 1
			tank = 0
		}
	}

	if sumGas < sumCost {
		return -1
	} else {
		return start
	}
}