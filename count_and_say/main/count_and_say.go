package main

import (
	"strconv"
)

func countAndSay(n int) string {
	res := "1"
	for i := 1; i < n; i++ {
		s := ""
		count := 1
		curr := res[0]
		for j := 1; j < len(res); j++ {
			if curr == res[j] {
				count++
			} else {
				s += strconv.Itoa(count) + string(curr)
				curr = res[j]
				count = 1
			}
		}
		s += strconv.Itoa(count) + string(curr)
		res = s
	}
	return res
}

func main() {
	println(countAndSay(5))
}
