package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/numbers-with-same-consecutive-differences/
 * https://www.youtube.com/watch?v=qFLE3KY7vRU&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=10
 */

func numsSameConsecDiff(N int, K int) []int {
	var ans []int
	if N == 1 {
		ans = append(ans, 0)
	}
	for i := 1; i <= 9; i++ {
		dfs(N-1, K, i, &ans)
	}
	return ans
}

func dfs(N int, K int, cur int, ans *[]int) {
	if N == 0 {
		*ans = append(*ans, cur)
		return
	}
	l := cur % 10
	if l+K <= 9 {
		dfs(N-1, K, cur*10+l+K, ans)
	}
	if l-K >= 0 && K != 0 {
		dfs(N-1, K, cur*10+l-K, ans)
	}
}

func main() {
	ans := numsSameConsecDiff(2, 1)
	spew.Dump(ans)
}
