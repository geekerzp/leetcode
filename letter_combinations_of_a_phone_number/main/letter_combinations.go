package main

func letterCombinations(digits string) []string {
	var ans []string
	if len(digits) == 0 {
		return ans
	}

	table := []string{"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"}
	combinations(&ans, digits, "", 0, table)
	return ans
}

func combinations(ans *[]string, digits string, curr string, index int, table []string) {
	if index == len(digits) {
		*ans = append(*ans, curr)
		return
	}

	temp := table[digits[index]-'0']
	for _, char := range temp {
		next := curr + string(char)
		combinations(ans, digits, next, index+1, table)
	}
}

func main() {
}
