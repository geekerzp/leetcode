package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func averageOfLevels(root *TreeNode) []float64 {
	var ans []float64
	curr := []*TreeNode{root}
	next := []*TreeNode{}

	for len(curr) > 0 {
		var sum float64
		for _, child := range curr {
			sum += float64(child.Val)
			if child.Left != nil {
				next = append(next, child.Left)
			}
			if child.Right != nil {
				next = append(next, child.Right)
			}
		}

		ans = append(ans, sum/float64(len(curr)))
		curr = next
		next = []*TreeNode{}
	}

	return ans
}

func main() {

}
