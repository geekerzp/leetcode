package main

/**
 * https://leetcode.com/problems/maximum-binary-tree/
 * https://www.youtube.com/watch?v=oHnT9zTsXTg&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=43&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	idx, max := maxIntSlice(nums)
	root := &TreeNode{Val: max}
	root.Left = constructMaximumBinaryTree(nums[0:idx])
	root.Right = constructMaximumBinaryTree(nums[idx+1:])
	return root
}

func maxIntSlice(v []int) (int, int) {
	var max int
	var idx int

	for i, e := range v {
		if i == 0 || e > max {
			max = e
			idx = i
		}
	}

	return idx, max
}

func main() {}
