package main

type Index int

const (
	GOOD Index = iota
	BAD
	UNKNOWN
)

var memo []Index

func canJumpFromPosition(position int, nums []int) bool {
	if memo[position] != UNKNOWN {
		if memo[position] == GOOD {
			return true
		} else {
			return false
		}
	}

	var furthestJump int
	if position + nums[position] < len(nums) - 1 {
		furthestJump = position + nums[position]
	} else {
		furthestJump = len(nums) - 1
	}

	for nexPosition := position + 1; nexPosition <= furthestJump; nexPosition++ {
		if canJumpFromPosition(nexPosition, nums) {
			memo[position] = GOOD
			return true
		}
	}

	memo[position] = BAD
	return false
}

func canJump(nums []int) bool {
	memo = make([]Index, len(nums))
	for i := 0; i < len(memo); i++ {
		memo[i] = UNKNOWN
	}
	memo[len(memo) - 1] = GOOD
	return canJumpFromPosition(0, nums)
}

func main() {
	canJump([]int{2,3,1,1,4})
}