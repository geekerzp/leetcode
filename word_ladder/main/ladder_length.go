package main

import (
	"github.com/davecgh/go-spew/spew"
)

func ladderLength(beginWord string, endWord string, wordList []string) int {
	marked := make(map[string]bool)
	for _, word := range wordList {
		marked[word] = false
	}

	marked[beginWord] = true
	toVisit := []string{beginWord}
	dist := 1
	for len(toVisit) != 0 {
		num := len(toVisit)
		for i := 0; i < num; i++ {
			var word string
			word, toVisit = toVisit[0], toVisit[1:len(toVisit)]
			if word == endWord {
				return dist
			}
			bfs(word, marked, &toVisit)
		}
		dist++
	}

	return 0
}

func bfs(word string, marked map[string]bool, toVisit *[]string) {
	for i, _ := range word {
		for j := 0; j < 26; j++ {
			search := replaceAtIndex(word, rune('a'+j), i)
			if visited, exists := marked[search]; exists && !visited {
				marked[search] = true
				*toVisit = append(*toVisit, search)
			}
		}
	}
}

func replaceAtIndex(in string, r rune, i int) string {
	out := []rune(in)
	out[i] = r
	return string(out)
}

func main() {
	spew.Dump(ladderLength("leet", "code", []string{"leet", "code", "lest", "leet", "lose", "code", "lode", "robe",
		"lost"}))
}
