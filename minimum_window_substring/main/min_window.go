package main

import (
	"github.com/davecgh/go-spew/spew"
	"math"
)

func minWindow(s string, t string) string {
	byteMap := make(map[byte]int)
	for i := 0; i < len(t); i++ {
		byteMap[t[i]]++
	}

	counter, begin, end, length, head := len(t), 0, 0, math.MaxInt64, 0

	for end < len(s) {
		if count, exists := byteMap[s[end]]; exists && count > 0 {
			counter--
		}
		if _, exists := byteMap[s[end]]; exists {
			byteMap[s[end]]--
		}
		end++

		for counter == 0 {
			if end-begin < length {
				head = begin
				length = end - begin
			}
			if count, exists := byteMap[s[begin]]; exists && count == 0 {
				counter++
			}
			if _, exists := byteMap[s[begin]]; exists {
				byteMap[s[begin]]++
			}
			begin++
		}
	}

	if length == math.MaxInt64 {
		return ""
	} else {
		return s[head : head+length]
	}
}

func main() {
	spew.Dump(minWindow("ADOBECODEBANC", "ABC"))
}
