package main

func twoSum(numbers []int, target int) []int {
	indice := make([]int, 2)
	if numbers == nil || len(numbers) < 2 {
		return indice
	}
	left, right := 0, len(numbers)-1
	for left < right {
		val := numbers[left] + numbers[right]
		if val == target {
			indice[0] = left + 1
			indice[1] = right + 1
			break
		} else if val < target {
			left++
		} else {
			right--
		}
	}
	return indice
}
