package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/redundant-connection/
 * https://www.youtube.com/watch?v=4hJ721ce010&list=PLLuMmzMTgVK5gFVMpryw0LkJp4l9WTtdM&index=39&t=0s
 */

func findRedundantConnection(edges [][]int) []int {
	up := make([]int, len(edges)+1)
	rank := make([]int, len(edges)+1)

	for _, edge := range edges {
		if !union(up, rank, edge[0], edge[1]) {
			return edge
		}
	}
	return nil
}

func find(up []int, x int) int {
	if up[x] == 0 {
		return x
	} else {
		up[x] = find(up, up[x])
		return up[x]
	}
}

func union(up []int, rank []int, x int, y int) bool {
	reprX := find(up, x)
	reprY := find(up, y)
	if reprX == reprY {
		return false
	}
	if rank[reprX] == rank[reprY] {
		rank[reprX]++
		up[reprY] = reprX
	} else if rank[reprX] > rank[reprY] {
		up[reprY] = reprX
	} else {
		up[reprX] = reprY
	}
	return true
}

func main() {
	ans := findRedundantConnection([][]int{{1, 2}, {2, 3}, {3, 4}, {1, 4}, {1, 5}})
	spew.Dump(ans)
}
