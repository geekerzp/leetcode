package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func zigzagLevelOrder(root *TreeNode) [][]int {
	var result [][]int
	var queue []*TreeNode

	if root == nil {
		return result
	}

	queue = append(queue, root)

	for len(queue) != 0 {
		var levelNodes []int

		nodes := len(queue)
		for i := 0; i < nodes; i++ {
			current := queue[0]
			queue = queue[1:len(queue)]

			if len(result)%2 == 0 {
				levelNodes = append(levelNodes, current.Val)
			} else {
				levelNodes = append([]int{current.Val}, levelNodes...)
			}

			if current.Left != nil {
				queue = append(queue, current.Left)
			}

			if current.Right != nil {
				queue = append(queue, current.Right)
			}
		}

		result = append(result, levelNodes)
	}

	return result
}
