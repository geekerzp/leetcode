package main

type ListNode struct {
	Val int
	Next *ListNode
}

func getIntersectionNode(headA *ListNode, headB *ListNode) *ListNode {
	ans := make(map[*ListNode]bool)

	for head := headA; head != nil; head = head.Next {
		ans[head] = true
	}

	for head := headB; head != nil; head = head.Next {
		if ans[head] {
			return head
		}
	}

	return nil
}

func main() {
	a1 := ListNode{Val: 1}
	a2 := ListNode{Val: 2}
	b1 := ListNode{Val: 3}
	b2 := ListNode{Val: 4}
	b3 := ListNode{Val: 5}
	c1 := ListNode{Val: 6}
	c2 := ListNode{Val: 7}
	c3 := ListNode{Val: 8}
	a1.Next = &a2
	a2.Next = &c1
	c1.Next = &c2
	c2.Next = &c3
	b1.Next = &b2
	b2.Next = &b3
	b3.Next = &c1

	getIntersectionNode(&a1, &b2)
}
