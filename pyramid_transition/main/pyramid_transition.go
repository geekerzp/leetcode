package main

// https://leetcode.com/problems/pyramid-transition-matrix/
func pyramidTransition(bottom string, allowed []string) bool {
	T := make([][]int, 7)
	for i := 0; i < len(T); i++ {
		T[i] = make([]int, 7)
	}

	for _, a := range allowed {
		T[a[0]-'A'][a[1]-'A'] |= 1 << (a[2] - 'A')
	}

	N := len(bottom)
	A := make([][]int, N)
	for i := 0; i < len(A); i++ {
		A[i] = make([]int, N)
	}
	for i, c := range bottom {
		A[N-1][i] = int(c - 'A')
	}

	return solve(&A, N-1, 0, &T)
}

func solve(A *[][]int, N int, i int, T *[][]int) bool {
	if N == 1 && i == 1 {
		return true
	} else if i == N {
		return solve(A, N-1, 0, T)
	} else {
		w := (*T)[(*A)[N][i]][(*A)[N][i+1]]
		for b := uint(0); b < 7; b++ {
			if w>>b&1 != 0 {
				(*A)[N-1][i] = int(b)
				if solve(A, N, i+1, T) {
					return true
				}
			}
		}
		return false
	}
}
