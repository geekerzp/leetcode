package main

import (
	"github.com/davecgh/go-spew/spew"
	"math"
)

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func minDiffInBST(root *TreeNode) int {
	prev, res := math.MinInt64, math.MaxInt64

	var stack []*TreeNode
	for root != nil || len(stack) > 0 {
		if root != nil {
			stack = append([]*TreeNode{root}, stack...)
			root = root.Left
		} else {
			root, stack = stack[0], stack[1:len(stack)]
			if prev != math.MinInt64 {
				if root.Val-prev < res {
					res = root.Val - prev
				}
			}
			prev = root.Val
			root = root.Right
		}
	}

	return res
}

func main() {
	root := &TreeNode{Val: 27}
	node := root
	node.Right = &TreeNode{Val: 34}
	node = node.Right
	node.Right = &TreeNode{Val: 58}
	node = node.Right
	node.Left = &TreeNode{Val: 50}
	node = node.Left
	node.Left = &TreeNode{Val: 44}
	spew.Dump(minDiffInBST(root))
}
