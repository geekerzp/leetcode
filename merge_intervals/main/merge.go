package main

import "sort"

type Interval struct {
	Start int
	End   int
}

func merge(intervals []Interval) []Interval {
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i].Start < intervals[j].Start
	})

	var merged []Interval
	for _, interval := range intervals {
		if len(merged) == 0 || merged[len(merged)-1].End < interval.Start {
			// if the list of merged intervals is empty or if the current
			// interval does not overlap  with previous, simply append it.
			merged = append(merged, interval)
		} else {
			// otherwise, there is overlap, so we merge the current and previous
			// intervals.
			if interval.End > merged[len(merged)-1].End {
				merged[len(merged)-1].End = interval.End
			}
		}
	}

	return merged
}

func main() {

}
