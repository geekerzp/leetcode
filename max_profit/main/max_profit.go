package main

import "math"

func maxProfit(prices []int) int {
	minprice := math.MaxInt64
	maxprofit := 0

	for _, p := range prices {
		if p < minprice {
			minprice = p
		} else if p - minprice > maxprofit {
			maxprofit = p - minprice
		}
	}

	return maxprofit
}
