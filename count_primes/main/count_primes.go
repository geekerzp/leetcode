package main

func countPrimes(n int) int {
	notPrime := make([]bool, n)
	count := 0

	for i := 2; i < n; i++ {
		if !notPrime[i] {
			count++
			for j := 2; i*j < n; j++ {
				notPrime[i*j] = true
			}
		}
	}

	return count
}
