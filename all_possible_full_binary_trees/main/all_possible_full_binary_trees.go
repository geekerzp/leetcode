package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func allPossibleFBT(N int) []*TreeNode {
	if N%2 == 0 {
		return nil
	}
	if N == 1 {
		return []*TreeNode{{Val: 0}}
	}
	var ans []*TreeNode
	for i := 1; i < N; i += 2 {
		for _, l := range allPossibleFBT(i) {
			for _, r := range allPossibleFBT(N - i - 1) {
				root := &TreeNode{Val: 0}
				root.Left = l
				root.Right = r
				ans = append(ans, root)
			}
		}
	}
	return ans
}
