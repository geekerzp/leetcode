package main

func maxProfit(prices []int) int {
	if len(prices) == 0 {
		return 0
	}

	valley := prices[0]
	peak := prices[0]
	maxprofit := 0
	for i := 0; i < len(prices) - 1; {
		for i < len(prices) - 1 && prices[i] >= prices[i+1] {
			i++
		}
		valley = prices[i]
		for i < len(prices) - 1 && prices[i] <= prices[i+1] {
			i++
		}
		peak = prices[i]
		maxprofit += peak - valley
	}
	return maxprofit
}

