package main

import (
	"sort"

	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/divide-array-in-sets-of-k-consecutive-numbers/
 * https://www.youtube.com/watch?v=7W45U4WLtzg&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=3&t=0s
 */

func isPossibleDivide(nums []int, k int) bool {
	freqNums := make(map[int]int)
	for _, num := range nums {
		freqNums[num] = freqNums[num] + 1
	}

	var sortedNums []int
	for num := range freqNums {
		sortedNums = append(sortedNums, num)
	}
	sort.Ints(sortedNums)

	for _, num := range sortedNums {
		if freqNums[num] > 0 {
			count := freqNums[num]
			for i := num; i < num+k; i++ {
				if freqNums[i] < count {
					return false
				}
				freqNums[i] = freqNums[i] - count
			}
		}
	}

	return true
}

func main() {
	ans := isPossibleDivide([]int{3, 2, 1, 2, 3, 4, 3, 4, 5, 9, 10, 11}, 3)
	spew.Dump(ans)
}
