package main

func singleNumber(nums []int) int {
	numsSet := make(map[int]bool)

	for _, num := range nums {
		numsSet[num] = true
	}

	var setSum int
	for num, _ := range numsSet {
		setSum += num
	}

	var numsSum int
	for _, num := range nums {
		numsSum += num
	}

	return 2 * setSum - numsSum
}