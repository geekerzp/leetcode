package main

/**
 * https://www.youtube.com/watch?v=q1zk8vZIDw0&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=10&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func subtreeWithAllDeepest(root *TreeNode) *TreeNode {
	return depth(root)[1].(*TreeNode)
}

func depth(root *TreeNode) []interface{} {
	if root == nil {
		return []interface{}{-1, nil}
	}
	l := depth(root.Left)
	r := depth(root.Right)
	depth := max(l[0].(int), r[0].(int))
	var node *TreeNode
	if l[0] == r[0] {
		node = root
	} else if l[0].(int) > r[0].(int) {
		node = l[1].(*TreeNode)
	} else {
		node = r[1].(*TreeNode)
	}
	return []interface{}{depth + 1, node}
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
