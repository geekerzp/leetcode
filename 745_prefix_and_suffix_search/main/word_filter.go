package main

import "github.com/davecgh/go-spew/spew"

/**
 * https://leetcode.com/problems/prefix-and-suffix-search/
 *
 */

type WordFilter struct {
	trie *Trie
}

type TrieNode struct {
	Index    int
	IsWord   bool
	Children []*TrieNode
}

type Trie struct {
	root *TrieNode
}

func (t *Trie) insert(word string, index int) {
	p := t.root
	for _, c := range word {
		if p.Children[c-'a'] == nil {
			p.Children[c-'a'] = NewTrieNode()
		}
		p = p.Children[c-'a']
		p.Index = index
	}
	p.IsWord = true
}

func (t *Trie) find(prefix string) *TrieNode {
	p := t.root
	for _, c := range prefix {
		p = p.Children[c-'a']
		if p == nil {
			break
		}
	}
	return p
}

// Returns the index of word that has the prefix.
func (t *Trie) startsWith(prefix string) int {
	node := t.find(prefix)
	if node == nil {
		return -1
	}
	return node.Index
}

func NewTrie() *Trie {
	return &Trie{root: NewTrieNode()}
}

func NewTrieNode() *TrieNode {
	return &TrieNode{Children: make([]*TrieNode, 27)}
}

func Constructor(words []string) WordFilter {
	wf := WordFilter{trie: NewTrie()}
	for i, w := range words {
		key := "{" + w
		wf.trie.insert(key, i)
		for j := 0; j < len(w); j++ {
			key = string(w[len(w)-1-j]) + key
			wf.trie.insert(key, i)
		}
	}
	return wf
}

func (this *WordFilter) F(prefix string, suffix string) int {
	return this.trie.startsWith(suffix + "{" + prefix)
}

/**
 * Your WordFilter object will be instantiated and called as such:
 * obj := Constructor(words);
 * param_1 := obj.F(prefix,suffix);
 */

func main() {
	obj := Constructor([]string{"apple"})
	f1 := obj.F("a", "e")
	spew.Dump(f1)
}
