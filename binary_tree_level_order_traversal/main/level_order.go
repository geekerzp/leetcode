package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func levelOrder(root *TreeNode) [][]int {
	var queue []*TreeNode
	var res [][]int

	if root == nil {
		return res
	}

	queue = append(queue, root)

	for len(queue) != 0 {
		levelNum := len(queue)
		var subList []int
		for i := 0; i < levelNum; i++ {
			if queue[0].Left != nil {
				queue = append(queue, queue[0].Left)
			}
			if queue[0].Right != nil {
				queue = append(queue, queue[0].Right)
			}
			subList = append(subList, queue[0].Val)
			queue = queue[1:len(queue)]
		}
		res = append(res, subList)
	}

	return res
}
