package main

import "fmt"

func generateParenthesis(n int) []string {
	ans := &[]string{}
	backtrack(ans, "", 0, 0, n)
	return *ans
}

func backtrack(ans *[]string, cur string, open int, close int, max int) {
	if len(cur) == max*2 {
		*ans = append(*ans, cur)
		return
	}

	if open < max {
		backtrack(ans, fmt.Sprintf("%s(", cur), open+1, close, max)
	}
	if close < open {
		backtrack(ans, fmt.Sprintf("%s)", cur), open, close+1, max)
	}
}

func main() {
	println(generateParenthesis(3))
}
