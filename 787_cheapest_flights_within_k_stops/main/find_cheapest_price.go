package main

import (
	"math"

	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/cheapest-flights-within-k-stops/
 * https://www.youtube.com/watch?v=PLY-lbcxEjg&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=21
 */

func findCheapestPrice(n int, flights [][]int, src int, dst int, K int) int {
	dp := make([][]int, K+2)
	for i := range dp {
		dp[i] = make([]int, n)
	}
	for i := 0; i < len(dp); i++ {
		for j := 0; j < len(dp[0]); j++ {
			dp[i][j] = math.MaxInt32
		}
	}

	dp[0][src] = 0
	for i := 1; i <= K+1; i++ {
		dp[i][src] = 0
		for _, p := range flights {
			dp[i][p[1]] = min(dp[i][p[1]], dp[i-1][p[0]]+p[2])
		}
	}

	if dp[K+1][dst] >= math.MaxInt32 {
		return -1
	} else {
		return dp[K+1][dst]
	}
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func main() {
	ans := findCheapestPrice(3, [][]int{{0, 1, 100}, {1, 2, 100}, {0, 2, 500}}, 0, 2, 0)
	spew.Dump(ans)
}
