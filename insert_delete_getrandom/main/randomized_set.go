package main

import "math/rand"

type RandomizedSet struct {
	Nums []int
	Locs map[int]int
}

func Constructor() RandomizedSet {
	return RandomizedSet{Nums: []int{}, Locs: map[int]int{}}
}

func (this *RandomizedSet) Insert(val int) bool {
	if _, exists := this.Locs[val]; exists {
		return false
	}
	this.Locs[val] = len(this.Nums)
	this.Nums = append(this.Nums, val)
	return true
}

func (this *RandomizedSet) Remove(val int) bool {
	if _, exists := this.Locs[val]; !exists {
		return false
	}
	loc := this.Locs[val]
	if loc < len(this.Nums)-1 {
		last := this.Nums[len(this.Nums)-1]
		this.Nums[loc] = last
		this.Locs[last] = loc
	}
	delete(this.Locs, val)
	this.Nums = this.Nums[0 : len(this.Nums)-1]
	return true
}

func (this *RandomizedSet) GetRandom() int {
	return this.Nums[rand.Intn(len(this.Nums))]
}
