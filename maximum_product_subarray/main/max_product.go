package main

import (
	"github.com/davecgh/go-spew/spew"
)

func maxProduct(nums []int) int {
	// store the result that is the max we have found so far
	res := nums[0]

	// max/min stores the max/min product of
	// subarray that ends with the current number nums[i]
	max, min := res, res
	for i := 1; i < len(nums); i++ {
		// multipled by a negative makes big number smaller, smaller number bigger
		// so we redefine the extremums by swapping them
		if nums[i] < 0 {
			max, min = min, max
		}

		// max/min product the current number is either the current number itself
		// or the max/min by previous number times the current one
		max = maxInt(nums[i], nums[i]*max)
		min = minInt(nums[i], nums[i]*min)

		// the newly computed max value is a candicate for our global result
		res = maxInt(res, max)
	}

	return res
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func minInt(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func main() {
	spew.Dump(maxProduct([]int{0, 2}))
}
