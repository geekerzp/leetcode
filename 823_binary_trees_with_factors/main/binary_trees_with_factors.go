package main

import (
	"sort"
)

func numFactoredBinaryTrees(A []int) int {
	kMod := 1000000007
	dp := make(map[int]int)
	sort.Ints(A)
	for i := 0; i < len(A); i++ {
		dp[A[i]] = 1
		for j := 0; j < i; j++ {
			if A[i]%A[j] == 0 {
				if _, exists := dp[A[i]/A[j]]; exists {
					dp[A[i]] += (dp[A[j]] * dp[A[i]/A[j]]) % kMod
				}
			}
		}
	}
	var ans int
	for _, v := range dp {
		ans += v
	}
	return ans % kMod
}

func main() {
	numFactoredBinaryTrees([]int{2, 4})
}
