package main

import "strconv"

func numDecodings(s string) int {
	n := len(s)
	if n == 0 {
		return 0
	}

	memo := make([]int, n+1)
	memo[n] = 1
	if s[n-1] != '0' {
		memo[n-1] = 1
	} else {
		memo[n-1] = 0
	}

	for i := n - 2; i >= 0; i-- {
		if s[i] == '0' {
			continue
		} else {
			if n, _ := strconv.Atoi(s[i:i+2]); n <= 26 {
				memo[i] = memo[i+1] + memo[i+2]
			} else {
				memo[i] = memo[i+1]
			}
		}
	}

	return memo[0]
}
