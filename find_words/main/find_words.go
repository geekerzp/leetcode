package main

import (
	"regexp"
)

func findWords(words []string) []string {
	var matched []string

	valid := regexp.MustCompile(`(?i)^([qwertyuiop]*|[asdfghjkl]*|[zxcvbnm]*)$`)

	for _, word := range words {
		if valid.Match([]byte(word)) {
			matched = append(matched, word)
		}
	}

	return matched
}