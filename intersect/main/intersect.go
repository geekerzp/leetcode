package main

func intersect(nums1 []int, nums2 []int) []int {
	numsMap := make(map[int]int)
	var res []int

	for _, n := range nums1 {
		if count, exists := numsMap[n]; exists {
			numsMap[n] = count + 1
		} else {
			numsMap[n] = 1
		}
	}

	for _, n := range nums2 {
		if count, exists := numsMap[n]; exists && count > 0 {
			res = append(res, n)
			numsMap[n] = count - 1
		}
	}

	return res
}
