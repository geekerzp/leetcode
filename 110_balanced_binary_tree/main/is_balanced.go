package main

/**
 * https://leetcode.com/problems/balanced-binary-tree/
 * https://www.youtube.com/watch?v=C75oWiy0bWM&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=47&t=0s
 */

import (
	"math"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func isBalanced(root *TreeNode) bool {
	if root == nil {
		return true
	}
	leftHeight := height(root.Left)
	rightHeight := height(root.Right)
	return absInt(leftHeight-rightHeight) <= 1 && isBalanced(root.Left) && isBalanced(root.Right)
}

func height(root *TreeNode) int {
	if root == nil {
		return 0
	}
	return maxInt(height(root.Left), height(root.Right)) + 1
}

func absInt(x int) int {
	return int(math.Abs(float64(x)))
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {}
