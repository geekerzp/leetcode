package main

type MinStack struct {
	Head *Node
}

type Node struct {
	Val int
	Min int
	Next *Node
}

/** initialize your data structure here. */
func Constructor() MinStack {
	return MinStack{}
}


func (this *MinStack) Push(x int)  {
	if this.Head == nil {
		this.Head = &Node{Val: x, Min: x}
	} else {
		var min int
		if x < this.Head.Min {
			min = x
		} else {
			min = this.Head.Min
		}
		this.Head = &Node{Val: x, Min: min, Next: this.Head}
	}
}


func (this *MinStack) Pop()  {
	this.Head = this.Head.Next
}

func (this *MinStack) Top() int {
	return this.Head.Val
}

func (this *MinStack) GetMin() int {
	return this.Head.Min
}

/**
 * Your MinStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(x);
 * obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.GetMin();
 */
func main() {
	minStack := Constructor()
	minStack.Push(-2)
	minStack.Push(0)
	minStack.Push(-3)
	println(minStack.GetMin())
	minStack.Pop()
	println(minStack.Top())
	println(minStack.GetMin())
}
