package main

import "github.com/davecgh/go-spew/spew"

func plusOne(digits []int) []int {
	for i := len(digits)-1; i >= 0; i-- {
		if digits[i] < 9 {
			digits[i]++
			return digits
		} else {
			digits[i] = 0
		}
	}
	digits = append([]int{1}, digits...)
	return digits
}

func main() {
	spew.Dump(plusOne([]int{9, 9}))
}
