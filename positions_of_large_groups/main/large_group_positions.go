package main

import "github.com/davecgh/go-spew/spew"

func largeGroupPositions(S string) [][]int {
	var ans [][]int
	i, N := 0, len(S) // i is the start of each group
	for j := 0; j < N; j++ {
		if j == N-1 || S[j] != S[j+1] {
			// Here, [i, j] represents a group.
			if j-i+1 >= 3 {
				ans = append(ans, []int{i, j})
			}
			i = j + 1
		}
	}
	return ans
}

func main() {
	spew.Dump(largeGroupPositions("abcdddeeeeaabbbcd"))
}
