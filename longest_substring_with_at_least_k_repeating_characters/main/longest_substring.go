package main

func longestSubstring(s string, k int) int {
	max := 0

	for h := 1; h <= 26; h++ {
		i, j, unique, noLessThanK := 0, 0, 0, 0
		counts := make([]int, 26)

		for j < len(s) {
			if unique <= h {
				idx := s[j] - 'a'
				if counts[idx] == 0 {
					unique++
				}
				counts[idx]++
				if counts[idx] == k {
					noLessThanK++
				}
				j++
			} else {
				idx := s[i] - 'a'
				if counts[idx] == k {
					noLessThanK--
				}
				counts[idx]--
				if counts[idx] == 0 {
					unique--
				}
				i++
			}
			if unique == h && unique == noLessThanK {
				if j-i > max {
					max = j - i
				}
			}
		}
	}

	return max
}

func main() {
	longestSubstring("ababbc", 2)
}
