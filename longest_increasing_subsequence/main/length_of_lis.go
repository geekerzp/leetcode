package main

import "math"

func lengthOfLIS(nums []int) int {
	if len(nums) == 0 {
		return 0
	}
	dp := make([]int, len(nums))
	dp[0] = 1
	maxans := 1
	for i := 1; i < len(nums); i++ {
		maxval := 0
		for j := 0; j < i; j++ {
			if nums[i] > nums[j] {
				maxval = int(math.Max(float64(maxval), float64(dp[j])))
			}
		}
		dp[i] = maxval + 1
		maxans = int(math.Max(float64(maxans), float64(dp[i])))
	}
	return maxans
}
