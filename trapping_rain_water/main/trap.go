package main

func trap(height []int) int {
	size := len(height)
	if size <= 0 {
		return 0
	}

	leftMax, rightMax := make([]int, size), make([]int, size)

	leftMax[0] = height[0]
	for i := 1; i < size; i++ {
		if height[i] > leftMax[i-1] {
			leftMax[i] = height[i]
		} else {
			leftMax[i] = leftMax[i-1]
		}
	}

	rightMax[size-1] = height[size-1]
	for i := size - 2; i >= 0; i-- {
		if height[i] > rightMax[i+1] {
			rightMax[i] = height[i]
		} else {
			rightMax[i] = rightMax[i+1]
		}
	}

	var ans int
	for i := 1; i < size-1; i++ {
		if leftMax[i] < rightMax[i] {
			ans += leftMax[i] - height[i]
		} else {
			ans += rightMax[i] - height[i]
		}
	}

	return ans
}
