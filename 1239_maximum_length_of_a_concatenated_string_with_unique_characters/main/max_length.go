package main

import (
	"math/bits"
)

/**
 * https://leetcode.com/problems/maximum-length-of-a-concatenated-string-with-unique-characters/
 * https://www.youtube.com/watch?v=N7womGmLXh8&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=5
 */

func maxLength(arr []string) int {
	var a []uint

	for _, x := range arr {
		var mask uint
		for i := 0; i < len(x); i++ {
			mask |= 1 << (x[i] - 'a')
		}
		if bits.OnesCount(mask) == len(x) {
			a = append(a, mask)
		}
	}

	var ans int
	dfs(0, 0, a, &ans)
	return ans
}

func dfs(s int, mask uint, a []uint, ans *int) {
	*ans = max(*ans, bits.OnesCount(mask))
	for i := s; i < len(a); i++ {
		if mask&a[i] == 0 {
			dfs(i+1, mask|a[i], a, ans)
		}
	}
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {
	ans := maxLength([]string{"abcdefghijklmnopqrstuvwxyz"})
	println(ans)
}
