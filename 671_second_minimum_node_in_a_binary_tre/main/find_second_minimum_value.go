package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func findSecondMinimumValue(root *TreeNode) int {
	return dfs(root, root.Val)
}

func dfs(root *TreeNode, s1 int) int {
	if root == nil {
		return -1
	}
	if root.Val > s1 {
		return root.Val
	}
	sl := dfs(root.Left, s1)
	sr := dfs(root.Right, s1)
	if sl == -1 {
		return sr
	}
	if sr == -1 {
		return sl
	}

	return minInt(sl, sr)
}

func minInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func main() {}
