package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func constructFromPrePost(pre []int, post []int) *TreeNode {
	size := len(pre)
	if size <= 0 {
		return nil
	}
	root := &TreeNode{Val: pre[0]}
	if size == 1 {
		return root
	}
	i := 0
	for post[i] != pre[1] {
		i++
	}
	l := i + 1
	root.Left = constructFromPrePost(pre[1:l+1], post[0:i+1])
	root.Right = constructFromPrePost(pre[l+1:], post[i+1:size-1])
	return root
}
