package main

func isValid(s string) bool {
	var stack []rune
	for _, c := range s {
		if c == '(' {
			stack = append(stack, ')')
		} else if c == '{' {
			stack = append(stack, '}')
		} else if c == '[' {
			stack = append(stack, ']')
		} else {
			if len(stack) == 0 {
				return false
			}
			if stack[len(stack)-1] != c {
				return false
			} else {
				stack = stack[:len(stack)-1]
			}
		}
	}
	return len(stack) == 0
}