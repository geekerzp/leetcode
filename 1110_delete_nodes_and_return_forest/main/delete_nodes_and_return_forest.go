package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func delNodes(root *TreeNode, to_delete []int) []*TreeNode {
	var ans []*TreeNode
	root = process(root, to_delete, &ans)

	if root != nil {
		ans = append(ans, root)
	}

	return ans
}

func process(root *TreeNode, to_delete []int, ans *[]*TreeNode) *TreeNode {
	if root == nil {
		return nil
	}

	root.Left = process(root.Left, to_delete, ans)
	root.Right = process(root.Right, to_delete, ans)

	if !contains(to_delete, root.Val) {
		return root
	}

	if root.Left != nil {
		*ans = append(*ans, root.Left)
	}

	if root.Right != nil {
		*ans = append(*ans, root.Right)
	}

	return nil
}

func contains(a []int, x int) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func main() {
	t := &TreeNode{
		Val: 1,
		Left: &TreeNode{
			Val:   2,
			Left:  &TreeNode{Val: 4},
			Right: &TreeNode{Val: 5}},
		Right: &TreeNode{
			Val:   3,
			Left:  &TreeNode{Val: 6},
			Right: &TreeNode{Val: 7}}}

	ans := delNodes(t, []int{3, 5})
	spew.Dump(ans)
}
