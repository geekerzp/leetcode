package main

import "sort"

func isAnagram(s string, t string) bool {
	if len(s) != len(t) {
		return false
	}
	str1 := []rune(s)
	str2 := []rune(t)
	sort.Slice(str1, func(i, j int) bool {
		return str1[i] < str1[j]
	})
	sort.Slice(str2, func(i, j int) bool {
		return str2[i] < str2[j]
	})

	return string(str1) == string(str2)
}
