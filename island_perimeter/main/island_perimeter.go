package main

func islandPerimeter(grid [][]int) int {
	islands, neighbours := 0, 0

	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			if grid[i][j] == 1 {
				islands++
				if i < len(grid)-1 && grid[i+1][j] == 1 {
					neighbours++
				}
				if j < len(grid[i])-1 && grid[i][j+1] == 1 {
					neighbours++
				}
			}
		}
	}

	return islands*4 - neighbours*2
}
