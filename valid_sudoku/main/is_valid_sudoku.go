package main

import (
	"github.com/davecgh/go-spew/spew"
)

func isValidSudoku(board [][]byte) bool {
	for i := 0; i < 9; i++ {
		rows := make(map[byte]bool)
		columns := make(map[byte]bool)
		cube := make(map[byte]bool)

		for j := 0; j < 9; j++ {
			if rows[board[i][j]] {
				return false
			}
			if board[i][j] != '.' {
				rows[board[i][j]] = true
			}

			if columns[board[j][i]] {
				return false
			}
			if board[j][i] != '.' {
				columns[board[j][i]] = true
			}

			rowIndex := 3 * (i / 3)
			colIndex := 3 * (i % 3)

			if cube[board[rowIndex+j/3][colIndex+j%3]] {
				return false
			}
			if board[rowIndex+j/3][colIndex+j%3] != '.' {
				cube[board[rowIndex+j/3][colIndex+j%3]] = true
			}
		}
	}

	return true
}

func main() {
	sudoku := [][]byte{
		{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'},
	}
	spew.Dump(isValidSudoku(sudoku))
}
