package main

func uniquePaths(m int, n int) int {
	path := make([][]int, m)
	for i := 0; i < m; i++ {
		path[i] = make([]int, n)
	}

	for i := 0; i < m; i++ {
		path[i][0] = 1
	}
	for j := 0; j < n; j++ {
		path[0][j] = 1
	}

	for i := 1; i < m; i++ {
		for j := 1; j < n; j++ {
			path[i][j] = path[i-1][j] + path[i][j-1]
		}
	}
	return path[m-1][n-1]
}

func main() {
	println(uniquePaths(7, 3))
}
