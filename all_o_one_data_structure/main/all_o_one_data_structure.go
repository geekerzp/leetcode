package main

import (
	"container/list"
	"github.com/davecgh/go-spew/spew"
)

type ListNode struct {
	Value int
	Keys  []string
}

type AllOne struct {
	L *list.List
	M map[string]*list.Element
}

/** Initialize your data structure here. */
func Constructor() AllOne {
	return AllOne{
		L: list.New(),
		M: make(map[string]*list.Element),
	}
}

/** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
func (s *AllOne) Inc(key string) {
	prevNode, ok := s.M[key]
	if !ok {
		if s.L.Len() == 0 || s.nodeValue(s.L.Front()) != 1 {
			s.L.PushFront(&ListNode{
				Value: 1,
				Keys:  []string{key},
			})
		} else {
			s.pushNodeKey(s.L.Front(), key)
		}
		s.M[key] = s.L.Front()
		return
	}

	currNode := prevNode.Next()
	if currNode == nil || s.nodeValue(currNode) != s.nodeValue(prevNode)+1 {
		currNode = s.L.InsertAfter(&ListNode{
			Value: s.nodeValue(prevNode) + 1,
			Keys:  []string{},
		}, prevNode)
	}
	s.pushNodeKey(currNode, key)
	s.M[key] = currNode

	s.removeNodeKey(prevNode, key)
	if len(s.nodeKeys(prevNode)) == 0 {
		s.L.Remove(prevNode)
	}
}

/** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
func (s *AllOne) Dec(key string) {
	curr, ok := s.M[key]
	if !ok {
		return
	}

	if s.nodeValue(curr) > 1 {
		prev := curr.Prev()
		if prev == nil || s.nodeValue(prev) != s.nodeValue(curr)-1 {
			prev = s.L.InsertBefore(&ListNode{
				Value: s.nodeValue(curr) - 1,
				Keys:  []string{},
			}, curr)
		}
		s.pushNodeKey(prev, key)
		s.M[key] = prev
	} else {
		delete(s.M, key)
	}

	s.removeNodeKey(curr, key)
	if len(s.nodeKeys(curr)) == 0 {
		s.L.Remove(curr)
	}
}

/** Returns one of the keys with maximal value. */
func (s *AllOne) GetMaxKey() string {
	element := s.L.Back()
	if element != nil {
		return s.nodeKeys(element)[0]
	}
	return ""
}

/** Returns one of the keys with Minimal value. */
func (s *AllOne) GetMinKey() string {
	element := s.L.Front()
	if element != nil {
		return s.nodeKeys(element)[0]
	}
	return ""
}

func (s *AllOne) pushNodeKey(element *list.Element, key string) {
	listNode := element.Value.(*ListNode)
	listNode.Keys = append(listNode.Keys, key)
}

func (s *AllOne) removeNodeKey(element *list.Element, key string) {
	listNode := element.Value.(*ListNode)
	var keys []string
	for _, k := range listNode.Keys {
		if k != key {
			keys = append(keys, k)
		}
	}
	listNode.Keys = keys
}

func (s *AllOne) nodeValue(element *list.Element) int {
	return element.Value.(*ListNode).Value
}

func (s *AllOne) nodeKeys(element *list.Element) []string {
	return element.Value.(*ListNode).Keys
}

/**
 * Your AllOne object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Inc(key);
 * obj.Dec(key);
 * param_3 := obj.GetMaxKey();
 * param_4 := obj.GetMinKey();
 */

func main() {
	obj := Constructor()
	obj.Inc("a")

	obj.Inc("b")

	obj.Inc("b")

	obj.Inc("b")

	obj.Inc("b")

	obj.Dec("b")
	spew.Dump(obj)
	obj.Dec("b")

	spew.Dump(obj.GetMaxKey())
	spew.Dump(obj.GetMinKey())
}
