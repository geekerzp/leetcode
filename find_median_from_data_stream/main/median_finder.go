package main

import (
	"container/heap"
	"github.com/davecgh/go-spew/spew"
)

/***
 * min-heap
 */
type MinHeap []int

func (h MinHeap) Len() int {
	return len(h)
}

func (h MinHeap) Less(i, j int) bool {
	return h[i] < h[j]
}

func (h MinHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *MinHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *MinHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

/***
 * max-heap
 */
type MaxHeap []int

func (h MaxHeap) Len() int {
	return len(h)
}

func (h MaxHeap) Less(i, j int) bool {
	return h[j] < h[i]
}

func (h MaxHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *MaxHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *MaxHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

type MedianFinder struct {
	Lo *MaxHeap
	Hi *MinHeap
}

func Constructor() MedianFinder {
	maxHeap := new(MaxHeap)
	heap.Init(maxHeap)
	minHeap := new(MinHeap)
	heap.Init(minHeap)
	return MedianFinder{maxHeap, minHeap}
}

func (this *MedianFinder) AddNum(num int) {
	heap.Push(this.Lo, num)
	heap.Push(this.Hi, heap.Pop(this.Lo))
	if this.Hi.Len() > this.Lo.Len() {
		heap.Push(this.Lo, heap.Pop(this.Hi))
	}
}

func (this *MedianFinder) FindMedian() float64 {
	if this.Lo.Len() > this.Hi.Len() {
		return float64((*this.Lo)[0])
	} else {
		return float64((*this.Lo)[0]+(*this.Hi)[0]) * 0.5
	}
}

func main() {
	finder := Constructor()
	finder.AddNum(1)
	finder.AddNum(2)
	spew.Dump(finder.FindMedian())
	finder.AddNum(3)
	spew.Dump(finder.FindMedian())
}
