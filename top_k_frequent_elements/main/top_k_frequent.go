package main

import "github.com/davecgh/go-spew/spew"

func topKFrequent(nums []int, k int) []int {
	bucket := make([][]int, len(nums)+1)
	frequencyMap := make(map[int]int)

	for _, n := range nums {
		if count, exists := frequencyMap[n]; exists {
			frequencyMap[n] = count + 1
		} else {
			frequencyMap[n] = 1
		}
	}

	for k, v := range frequencyMap {
		bucket[v] = append(bucket[v], k)
	}

	var res []int
	for pos := len(bucket) - 1; pos >= 0 && len(res) < k; pos-- {
		if bucket[pos] != nil {
			res = append(res, bucket[pos]...)
		}
	}
	return res
}

func main() {
	spew.Dump(topKFrequent([]int{1, 2}, 2))
}
