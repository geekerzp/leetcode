package main

import "sort"

/**
 * [1, 5, 1, 1, 6, 4]
 */
func wiggleSort(nums []int) {
	median := findKthLargest(nums, (len(nums)+1)/2)
	n := len(nums)

	left, i, right := 0, 0, n-1

	for i <= right {
		if nums[newIndex(i, n)] > median {
			swap(nums, newIndex(left, n), newIndex(i, n))
			left++
			i++
		} else if nums[newIndex(i, n)] < median {
			swap(nums, newIndex(right, n), newIndex(i, n))
			right--
		} else {
			i++
		}
	}
}

func findKthLargest(nums []int, k int) int {
	sort.Ints(nums)
	return nums[len(nums)-k]
}

func swap(nums []int, i, j int) {
	nums[i], nums[j] = nums[j], nums[i]
}

func newIndex(index, n int) int {
	return (1 + 2*index) % (n | 1)
}
