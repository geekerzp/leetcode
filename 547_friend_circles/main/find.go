package main

/**
 * https://leetcode.com/problems/friend-circles/
 * https://www.youtube.com/watch?v=HHiHno66j40&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=37
 */

func findCircleNum(M [][]int) int {
	n := len(M)
	visited := make([]int, n)
	var ans int

	for i := 0; i < n; i++ {
		if visited[i] == 1 {
			continue
		}
		dfs(M, i, n, visited)
		ans++
	}

	return ans
}

func dfs(M [][]int, curr int, n int, visited []int) {
	visited[curr] = 1
	for i := 0; i < n; i++ {
		if M[curr][i] == 1 && visited[i] == 0 {
			dfs(M, i, n, visited)
		}
	}
}

func main() {}
