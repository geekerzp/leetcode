package main

func digitSquareSum(n int) int {
	var sum, tmp int
	for n > 0 {
		tmp = n % 10
		sum += tmp * tmp
		n /= 10
	}
	return sum
}

func isHappy(n int) bool {
	slow, fast := n, n
	slow = digitSquareSum(slow)
	fast = digitSquareSum(fast)
	fast = digitSquareSum(fast)
	for slow != fast {
		slow = digitSquareSum(slow)
		fast = digitSquareSum(fast)
		fast = digitSquareSum(fast)
	}
	if slow == 1 {
		return true
	} else {
		return false
	}
}

func main() {
	println(isHappy(19))
}
