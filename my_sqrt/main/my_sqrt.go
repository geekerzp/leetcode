package main

import "math"

func mySqrt(x int) int {
	var ans, delta float64 = float64(x), 0.0001
	for math.Abs(math.Pow(ans, 2) - float64(x)) > delta {
		ans = (ans + float64(x) / ans) / 2
	}
	return int(ans)
}


