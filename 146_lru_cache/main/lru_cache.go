package main

import "container/list"

type CacheNode struct {
	Key   int
	Value int
}

type LRUCache struct {
	Capacity int
	NodeMap  map[int]*list.Element
	NodeList *list.List
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		Capacity: capacity,
		NodeMap:  make(map[int]*list.Element),
		NodeList: list.New(),
	}
}

func (c *LRUCache) Get(key int) int {
	node, ok := c.NodeMap[key]
	if !ok {
		return -1
	}
	c.NodeList.MoveToFront(node)
	cacheNode := node.Value.(*CacheNode)
	return cacheNode.Value
}

func (c *LRUCache) Put(key int, value int) {
	node, ok := c.NodeMap[key]
	if ok {
		cacheNode := node.Value.(*CacheNode)
		cacheNode.Value = value
		c.NodeList.MoveToFront(node)
		return
	}

	if c.NodeList.Len() == c.Capacity {
		lastNode := c.NodeList.Back()
		c.NodeList.Remove(lastNode)
		lastCacheNode := lastNode.Value.(*CacheNode)
		delete(c.NodeMap, lastCacheNode.Key)
	}

	c.NodeList.PushFront(&CacheNode{
		Key:   key,
		Value: value,
	})
	c.NodeMap[key] = c.NodeList.Front()
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
