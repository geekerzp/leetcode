package main

/*
 * https: //leetcode.com/problems/convert-bst-to-greater-tree/
 */

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func convertBST(root *TreeNode) *TreeNode {
	var sum int
	node := root

	for node != nil {
		if node.Right == nil {
			/*
					 * If there no right subtree, then we can visit this node and
				 	 * continue traversing left.
			*/
			sum += node.Val
			node.Val = sum
			node = node.Left
		} else {
			/*
			 * If there is a right subtree, then there is at least one node that
			 * has a greater value than the current one. therefore, we
			 * traverse that subtree first.
			 */
			succ := getSuccessor(node)
			if succ.Left == nil {
				/*
				 * If the left subtree is null, then we have never been here before.
				 */
				succ.Left = node
				node = node.Right
			} else {
				/*
				 * If there is a left subtree, it is a link that we created on a
				 * previous pass, so we should unlink it and visit this node.
				 */
				succ.Left = nil
				sum += node.Val
				node.Val = sum
				node = node.Left
			}
		}
	}

	return root
}

/*
 * Get the node with the smallest value greater than this one.
 */
func getSuccessor(node *TreeNode) *TreeNode {
	succ := node.Right
	for succ.Left != nil && succ.Left != node {
		succ = succ.Left
	}
	return succ
}
