package main

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

type ListNode struct {
	Val  int
	Next *ListNode
}

func splitListToParts(root *ListNode, k int) []*ListNode {
	var length int
	for head := root; head != nil; head = head.Next {
		length++
	}

	l := length / k
	r := length % k
	var prev *ListNode
	head := root

	ans := make([]*ListNode, k)
	for i := 0; i < k; i++ {
		ans[i] = head
		for j := 0; j < l; j++ {
			prev = head
			head = head.Next
		}
		if r > 0 {
			prev = head
			head = head.Next
		}
		if prev != nil {
			prev.Next = nil
		}
		r--
	}
	return ans
}
