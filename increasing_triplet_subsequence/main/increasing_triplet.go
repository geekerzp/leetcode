package main

import "math"

func increasingTriplet(nums []int) bool {
	c1, c2 := math.MaxInt64, math.MaxInt64
	for _, x := range nums {
		if x <= c1 {
			c1 = x
		} else if x <= c2 {
			c2 = x
		} else {
			return true
		}
	}
	return false
}
