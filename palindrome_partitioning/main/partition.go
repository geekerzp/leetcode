package main

func partition(s string) [][]string {
	var currList []string
	var resultList [][]string
	backTrack(s, 0, &currList, &resultList)
	return resultList
}

func isPalindrome(str string, l int, r int) bool {
	if l == r {
		return true
	}

	for l < r {
		if str[l] != str[r] {
			return false
		}
		l++
		r--
	}

	return true
}

func backTrack(s string, l int, currList *[]string, resultList *[][]string) {
	if len(*currList) > 0 && l >= len(s) {
		*resultList = append(*resultList, append([]string{}, *currList...))
	}

	for i := l; i < len(s); i++ {
		if isPalindrome(s, l, i) {
			if l == i {
				*currList = append(*currList, string(s[i]))
			} else {
				*currList = append(*currList, s[l:i+1])
			}
			backTrack(s, i+1, currList, resultList)
			*currList = (*currList)[0 : len(*currList)-1]
		}
	}
}
