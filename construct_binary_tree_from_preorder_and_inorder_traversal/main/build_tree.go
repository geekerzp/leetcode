package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func buildTree(preorder []int, inorder []int) *TreeNode {
	return doBuildTree(0, 0, len(inorder)-1, preorder, inorder)
}

func doBuildTree(preStart int, inStart int, inEnd int, preorder []int, inorder []int) *TreeNode {
	if preStart > len(preorder)-1 || inStart > inEnd {
		return nil
	}
	root := &TreeNode{Val: preorder[preStart]}
	var inIndex int
	for i := inStart; i <= inEnd; i++ {
		if inorder[i] == root.Val {
			inIndex = i
		}
	}
	root.Left = doBuildTree(preStart+1, inStart, inIndex-1, preorder, inorder)
	root.Right = doBuildTree(preStart+inIndex-inStart+1, inIndex+1, inEnd, preorder, inorder)
	return root
}
