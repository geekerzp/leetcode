package main

func numJewelsInStones(J string, S string) int {
	res := 0

	mapJ := make(map[rune]bool, len(J))
	for _, i := range J {
		mapJ[i] = true
	}

	for _, j := range S {
		if mapJ[j] {
			res++
		}
	}

	return res
}