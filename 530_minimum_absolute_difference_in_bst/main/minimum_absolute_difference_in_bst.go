package main

import "math"

/**
 * https://www.youtube.com/watch?v=0JHrHh_mIIw&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=19
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func getMinimumDifference(root *TreeNode) int {
	var sorted []int
	inorder(root, &sorted)
	minDiff := math.MaxInt64
	for i := 1; i < len(sorted); i++ {
		minDiff = min(minDiff, sorted[i]-sorted[i-1])
	}
	return minDiff
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func inorder(root *TreeNode, sorted *[]int) {
	if root == nil {
		return
	}
	inorder(root.Left, sorted)
	*sorted = append(*sorted, root.Val)
	inorder(root.Right, sorted)
}
