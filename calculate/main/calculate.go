package main

import "unicode"

func calculate(s string) int {
	var num  = 0
	var sign  = '+'
	var stack []int
	var n int

	for i, c := range s {
		if unicode.IsNumber(c) {
			num = num * 10 + int(c) - '0'
		}
		if (!unicode.IsNumber(c) && c != ' ') || i == len(s) - 1 {
			if sign == '+' {
				stack = append(stack, num)
			}
			if sign == '-' {
				stack = append(stack, -num)
			}
			if sign == '*' {
				n, stack = stack[len(stack)-1], stack[:len(stack)-1]
				stack = append(stack, n * num)
			}
			if sign == '/' {
				n, stack = stack[len(stack)-1], stack[:len(stack)-1]
				stack = append(stack, n / num)
			}
		}
		if !unicode.IsNumber(c) && c != ' ' {
			sign = c
			num = 0
		}
	}

	var res int
	for _, val := range stack {
		res += val
	}

	return res
}

func main() {
	println(calculate(" 3/2 "))
}
