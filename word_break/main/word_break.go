package main

import "github.com/davecgh/go-spew/spew"

func wordBreak(s string, wordDict []string) bool {
	dp := make([]bool, len(s)+1)
	dp[0] = true

	for i := 1; i <= len(s); i++ {
		for j := 0; j < i; j++ {
			if dp[j] && Contains(wordDict, s[j:i]) {
				dp[i] = true
				break
			}
		}
	}

	spew.Dump(dp)

	return dp[len(s)]
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func main() {
	spew.Dump(wordBreak("leetcode", []string{"leet", "code"}))
}
