package main

import (
	"regexp"
	"strings"
)

func isPalindrome(s string) bool {
	str := regexp.MustCompile("[^A-Za-z0-9]").ReplaceAllString(s, "")
	str = strings.ToLower(str)

	var temp []byte
	for i := len(str) - 1; i >= 0; i-- {
		temp = append(temp, str[i])
	}
	reverseStr := string(temp)

	return str == reverseStr
}