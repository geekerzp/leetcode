package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/sudoku-solver/
 * https://www.youtube.com/watch?v=ucugbKwjtRs&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=31
 */

func solveSudoku(board [][]byte) {
	rows := make([][]int, 9)
	for i := range rows {
		rows[i] = make([]int, 10)
	}
	cols := make([][]int, 9)
	for i := range cols {
		cols[i] = make([]int, 10)
	}
	boxes := make([][]int, 9)
	for i := range boxes {
		boxes[i] = make([]int, 10)
	}

	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			c := board[i][j]
			if c != '.' {
				n := int(c - '0')
				bx := j / 3
				by := i / 3
				rows[i][n] = 1
				cols[j][n] = 1
				boxes[by*3+bx][n] = 1
			}
		}
	}

	fill(board, 0, 0, rows, cols, boxes)
}

func fill(board [][]byte, x int, y int, rows [][]int, cols [][]int, boxes [][]int) bool {
	if y == 9 {
		return true
	}

	var nx, ny int
	nx = (x + 1) % 9
	if nx == 0 {
		ny = y + 1
	} else {
		ny = y
	}

	if board[y][x] != '.' {
		return fill(board, nx, ny, rows, cols, boxes)
	}

	for i := 1; i <= 9; i++ {
		bx := x / 3
		by := y / 3
		boxKey := by*3 + bx
		if rows[y][i] == 0 && cols[x][i] == 0 && boxes[boxKey][i] == 0 {
			rows[y][i] = 1
			cols[x][i] = 1
			boxes[boxKey][i] = 1
			board[y][x] = byte(i + '0')
			if fill(board, nx, ny, rows, cols, boxes) {
				return true
			}
			board[y][x] = '.'
			boxes[boxKey][i] = 0
			cols[x][i] = 0
			rows[y][i] = 0
		}
	}

	return false
}

func main() {
	board := [][]byte{
		{'.', '.', '9', '7', '4', '8', '.', '.', '.'},
		{'7', '.', '.', '.', '.', '.', '.', '.', '.'},
		{'.', '2', '.', '1', '.', '9', '.', '.', '.'},
		{'.', '.', '7', '.', '.', '.', '2', '4', '.'},
		{'.', '6', '4', '.', '1', '.', '5', '9', '.'},
		{'.', '9', '8', '.', '.', '.', '3', '.', '.'},
		{'.', '.', '.', '8', '.', '3', '.', '2', '.'},
		{'.', '.', '.', '.', '.', '.', '.', '.', '6'},
		{'.', '.', '.', '2', '7', '5', '9', '.', '.'}}

	solveSudoku(board)

	spew.Dump(board)
}
