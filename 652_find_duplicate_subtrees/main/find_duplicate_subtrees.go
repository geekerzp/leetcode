package main

import "github.com/davecgh/go-spew/spew"

/**
 * https://leetcode.com/problems/find-duplicate-subtrees/
 * https://www.youtube.com/watch?v=JLK92dbTt8k&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=19&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func findDuplicateSubtrees(root *TreeNode) []*TreeNode {
	ids := make(map[int64]int)
	counts := make(map[int]int)
	var ans []*TreeNode
	getId(root, ids, counts, &ans)
	return ans
}

func getId(root *TreeNode, ids map[int64]int, counts map[int]int, ans *[]*TreeNode) int {
	if root == nil {
		return 0
	}

	key := int64(root.Val)<<32 |
		int64(getId(root.Left, ids, counts, ans))<<16 |
		int64(getId(root.Right, ids, counts, ans))

	if _, exists := ids[key]; !exists {
		ids[key] = len(ids) + 1
	}
	id := ids[key]

	counts[id] = counts[id] + 1
	if counts[id] == 2 {
		*ans = append(*ans, root)
	}

	return id
}

func main() {
	root := &TreeNode{Val: 1}
	root.Left = &TreeNode{Val: 2}
	root.Right = &TreeNode{Val: 3}
	root.Left.Left = &TreeNode{Val: 4}
	root.Right.Left = &TreeNode{Val: 2}
	root.Right.Right = &TreeNode{Val: 4}
	root.Right.Left.Left = &TreeNode{Val: 4}

	spew.Dump(findDuplicateSubtrees(root))
}
