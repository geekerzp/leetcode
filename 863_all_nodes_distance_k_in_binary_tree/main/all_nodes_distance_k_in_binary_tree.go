package main

/**
 * https://www.youtube.com/watch?v=o1siL8eKCos&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=11&t=0s
 */

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func distanceK(root *TreeNode, target *TreeNode, K int) []int {
	var ans []int
	dis(root, target, K, &ans)
	return ans
}

// Returns the distance from root to target.
// Returns -1 if target does not in the tree.
func dis(root *TreeNode, target *TreeNode, K int, ans *[]int) int {
	if root == nil {
		return -1
	}

	if root == target {
		collect(target, K, ans)
		return 0
	}

	l := dis(root.Left, target, K, ans)
	r := dis(root.Right, target, K, ans)

	// Target in the left subtree
	if l >= 0 {
		if l == K-1 {
			*ans = append(*ans, root.Val)
		}
		// Collect nodes in right subtree with depth K - l - 2
		collect(root.Right, K-l-2, ans)
		return l + 1
	}

	// Target in the right subtree
	if r >= 0 {
		if r == K-1 {
			*ans = append(*ans, root.Val)
		}
		// Collect nodes in left subtree with depth K - r - 2
		collect(root.Left, K-r-2, ans)
		return r + 1
	}

	return -1
}

// Collect nodes that are d steps from root.
func collect(root *TreeNode, d int, ans *[]int) {
	if root == nil || d < 0 {
		return
	}
	if d == 0 {
		*ans = append(*ans, root.Val)
	}
	collect(root.Left, d-1, ans)
	collect(root.Right, d-1, ans)
}
