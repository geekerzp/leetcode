package main

import (
	"github.com/davecgh/go-spew/spew"
)

func gameOfLife(board [][]int) {
	if board == nil || len(board) == 0 || len(board[0]) == 0 {
		return
	}

	m, n := len(board), len(board[0])

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			lives := liveNeighbors(board, m, n, i, j)

			if board[i][j] == 1 && lives >= 2 && lives <= 3 {
				// 01 -> 11
				board[i][j] = 3
			}
			if board[i][j] == 0 && lives == 3 {
				// 00 -> 10
				board[i][j] = 2
			}
		}
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			board[i][j] >>= 1
		}
	}
}

func liveNeighbors(board [][]int, m int, n int, i int, j int) int {
	lives := 0
	for x := max(i-1, 0); x <= min(i+1, m-1); x++ {
		for y := max(j-1, 0); y <= min(j+1, n-1); y++ {
			lives += board[x][y] & 1
		}
	}
	lives -= board[i][j] & 1
	return lives
}

func max(m int, n int) int {
	if m > n {
		return m
	}
	return n
}

func min(m int, n int) int {
	if m < n {
		return m
	}
	return n
}

func main() {
	board := [][]int{
		{0, 1, 0},
		{0, 0, 1},
		{1, 1, 1},
		{0, 0, 0},
	}
	gameOfLife(board)
	spew.Dump(board)
}
