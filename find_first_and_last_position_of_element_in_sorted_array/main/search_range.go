package main

func searchRange(nums []int, target int) []int {
	targetRange := []int{-1, -1}

	leftIdx := extremeInsertionIndex(nums, target, true)

	if leftIdx == len(nums) || nums[leftIdx] != target {
		return targetRange
	}

	targetRange[0] = leftIdx
	targetRange[1] = extremeInsertionIndex(nums, target, false) - 1

	return targetRange
}

func extremeInsertionIndex(nums []int, target int, left bool) int {
	lo, hi := 0, len(nums)

	for lo < hi {
		mid := (lo + hi) / 2
		if nums[mid] > target || (left && nums[mid] == target) {
			hi = mid
		} else {
			lo = mid + 1
		}
	}

	return lo
}
