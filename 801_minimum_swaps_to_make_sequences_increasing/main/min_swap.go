package main

import (
	"math"
)

/**
 * https://leetcode.com/problems/minimum-swaps-to-make-sequences-increasing/
 * https://www.youtube.com/watch?v=__yxFFRQAl8&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=16
 */

func minSwap(A []int, B []int) int {
	n := len(A)

	keep := make([]int, n)
	for i := range keep {
		keep[i] = math.MaxInt64
	}
	swap := make([]int, n)
	for i := range swap {
		swap[i] = math.MaxInt64
	}

	keep[0] = 0
	swap[0] = 1

	for i := 1; i < n; i++ {
		if A[i] > A[i-1] && B[i] > B[i-1] {
			keep[i] = keep[i-1]
			swap[i] = swap[i-1] + 1
		}

		if B[i] > A[i-1] && A[i] > B[i-1] {
			keep[i] = min(keep[i], swap[i-1])
			swap[i] = min(swap[i], keep[i-1]+1)
		}
	}

	return min(keep[n-1], swap[n-1])
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func main() {
	minSwap([]int{1, 3, 5, 4}, []int{1, 2, 3, 7})
}
