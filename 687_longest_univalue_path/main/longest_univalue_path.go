package main

/**
 * https://leetcode.com/problems/longest-univalue-path/
 * https://www.youtube.com/watch?v=yX1hVhcHcH8&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=32&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func longestUnivaluePath(root *TreeNode) int {
	var ans int
	univaluePath(root, &ans)
	return ans
}

func univaluePath(root *TreeNode, ans *int) int {
	if root == nil {
		return 0
	}
	l := univaluePath(root.Left, ans)
	r := univaluePath(root.Right, ans)
	var pl, pr int
	if root.Left != nil && root.Left.Val == root.Val {
		pl = l + 1
	}
	if root.Right != nil && root.Right.Val == root.Val {
		pr = r + 1
	}
	*ans = maxInt(*ans, pl+pr)
	return maxInt(pl, pr)
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {}
