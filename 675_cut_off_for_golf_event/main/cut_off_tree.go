package main

import (
	"sort"

	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/cut-off-trees-for-golf-event/
 * https://www.youtube.com/watch?v=OFkLC30OxXM&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=40&t=0s
 */

func cutOffTree(forest [][]int) int {
	m := len(forest)
	n := len(forest[0])

	// {height, x, y}
	var trees [][]int
	for y := 0; y < m; y++ {
		for x := 0; x < n; x++ {
			if forest[y][x] > 0 {
				trees = append(trees, []int{forest[y][x], x, y})
			}
		}
	}

	// sort trees by height
	sort.Slice(trees, func(i, j int) bool {
		return trees[i][0] < trees[j][0]
	})

	var sx, sy, totalSteps int

	for _, point := range trees {
		tx := point[1]
		ty := point[2]

		steps := bfs(forest, sx, sy, tx, ty)
		if steps == -1 {
			return -1
		}

		totalSteps += steps

		sx = tx
		sy = ty
	}

	return totalSteps
}

func bfs(forest [][]int, sx int, sy int, tx int, ty int) int {
	m := len(forest)
	n := len(forest[0])

	dirs := [4][2]int{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}

	visited := make([][]int, m)
	for i := range visited {
		visited[i] = make([]int, n)
	}

	var q [][]int
	q = append(q, []int{sx, sy})

	var steps int
	for len(q) > 0 {
		newNodes := len(q)
		for newNodes > 0 {
			var point []int
			point, q = q[0], q[1:]
			cx, cy := point[0], point[1]

			// found the shortest path
			if cx == tx && cy == ty {
				return steps
			}

			for i := 0; i < 4; i++ {
				x := cx + dirs[i][0]
				y := cy + dirs[i][1]

				// out of bound or unwalkable or visited
				if x < 0 || x == n ||
					y < 0 || y == m ||
					forest[y][x] == 0 ||
					visited[y][x] == 1 {
					continue
				}

				visited[y][x] = 1
				// add new point into queue
				q = append(q, []int{x, y})
			}

			newNodes--
		}
		steps++
	}

	// impossible to reach
	return -1
}

func main() {
	ans := cutOffTree([][]int{
		{1, 2, 3},
		{0, 0, 4},
		{7, 6, 5}})

	spew.Dump(ans)
}
