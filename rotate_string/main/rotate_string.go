package main

func rotateString(A string, B string) bool {
	if len(A) != len(B) {
		return false
	}

	if len(A) == 0 {
		return true
	}

	search:
	for  s := 0; s < len(A); s++ {
		for i := 0; i < len(A); i++ {
			if (A[(s + i) % len(A)]) != B[i] {
				continue search
			}
		}
		return true
	}
	return false
}
