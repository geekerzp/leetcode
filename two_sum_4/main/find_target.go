package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func findTarget(root *TreeNode, k int) bool {
	var list []int
	inorder(root, &list)
	l, r := 0, len(list)-1
	for l < r {
		sum := list[l] + list[r]
		if sum == k {
			return true
		}
		if sum < k {
			l++
		} else {
			r--
		}
	}
	return false
}

func inorder(root *TreeNode, list *[]int) {
	if root == nil {
		return
	}
	inorder(root.Left, list)
	*list = append(*list, root.Val)
	inorder(root.Right, list)
}
