package main

import "github.com/davecgh/go-spew/spew"

func numIslands(grid [][]byte) int {
	if grid == nil || len(grid) == 0 || len(grid[0]) == 0 {
		return 0
	}

	m, n, res := len(grid), len(grid[0]), 0
	visited := make([][]bool, m)
	for i := range visited {
		visited[i] = make([]bool, n)
	}

	for i := 0; i < m; i++ {
		for j := 0; j < n; j++ {
			if grid[i][j] == '1' && !visited[i][j] {
				numIslandsDFS(grid, visited, i, j)
				res++
			}
		}
	}

	return res
}

func numIslandsDFS(grid [][]byte, visited [][]bool, x int, y int) {
	if x < 0 || x >= len(grid) {
		return
	}
	if y < 0 || y >= len(grid[0]) {
		return
	}
	if grid[x][y] != '1' || visited[x][y] {
		return
	}
	visited[x][y] = true
	numIslandsDFS(grid, visited, x-1, y)
	numIslandsDFS(grid, visited, x+1, y)
	numIslandsDFS(grid, visited, x, y-1)
	numIslandsDFS(grid, visited, x, y+1)
}

func main() {
	spew.Dump(numIslands([][]byte{
		{'1', '1', '0', '0', '0'},
		{'1', '1', '0', '0', '0'},
		{'0', '0', '1', '0', '0'},
		{'0', '0', '0', '1', '1'}}))
}
