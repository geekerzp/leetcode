package main

import (
	"math"
)

/**
 * https://www.youtube.com/watch?v=iE0tJ-8rPLQ&list=PLLuMmzMTgVK5gFVMpryw0LkJp4l9WTtdM&index=2&t=0s
 * https://leetcode.com/problems/find-the-city-with-the-smallest-number-of-neighbors-at-a-threshold-distance/
 */

func findTheCity(n int, edges [][]int, distanceThreshold int) int {
	dist := make([][]int, n)
	for i := range dist {
		dist[i] = make([]int, n)
	}
	for i := range dist {
		for j := range dist[i] {
			if i == j {
				dist[i][j] = 0
			} else {
				dist[i][j] = math.MaxInt32
			}
		}
	}
	for _, e := range edges {
		dist[e[0]][e[1]], dist[e[1]][e[0]] = e[2], e[2]
	}

	// floyd-warshall
	for k := 0; k < n; k++ {
		for i := 0; i < n; i++ {
			for j := 0; j < n; j++ {
				dist[i][j] = min(dist[i][j], dist[i][k]+dist[k][j])
			}
		}
	}

	ans := -1
	minCount := math.MaxInt64
	for i := 0; i < n; i++ {
		var count int
		for j := 0; j < n; j++ {
			if dist[i][j] > 0 && dist[i][j] <= distanceThreshold {
				count++
			}
		}
		if count <= minCount {
			minCount = count
			ans = i
		}
	}

	return ans
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func main() {
	ans := findTheCity(4, [][]int{{0, 1, 3}, {1, 2, 1}, {1, 3, 4}, {2, 3, 1}}, 4)
	println(ans)
}
