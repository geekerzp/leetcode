package main

/**
 * https://leetcode.com/problems/reconstruct-itinerary/
 * https://www.youtube.com/watch?v=4udFSOWQpdg&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=40&t=0s
 */

import (
	"container/heap"

	"github.com/davecgh/go-spew/spew"
)

type StringHeap []string

func (h StringHeap) Len() int           { return len(h) }
func (h StringHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h StringHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *StringHeap) Push(x interface{}) {
	*h = append(*h, x.(string))
}

func (h *StringHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func findItinerary(tickets [][]string) []string {
	var route []string
	trips := make(map[string]*StringHeap)

	for _, ticket := range tickets {
		if _, exists := trips[ticket[0]]; !exists {
			h := &StringHeap{}
			heap.Init(h)
			trips[ticket[0]] = h
		}
		heap.Push(trips[ticket[0]], ticket[1])
	}

	kStart := "JFK"

	visit(kStart, &trips, &route)

	var ans []string
	for i := range route {
		e := route[len(route)-1-i]
		ans = append(ans, e)
	}
	return ans
}

func visit(src string, trips *map[string]*StringHeap, route *[]string) {
	dests := (*trips)[src]
	for dests != nil && dests.Len() > 0 {
		// Get the smallest dest and remove the ticket
		dest := heap.Pop(dests).(string)
		// Visit dest
		visit(dest, trips, route)
	}
	*route = append(*route, src)
}

func main() {
	ans := findItinerary([][]string{
		[]string{"MUC", "LHR"},
		[]string{"JFK", "MUC"},
		[]string{"SFO", "SJC"},
		[]string{"LHR", "SFO"}})
	spew.Dump(ans)
}
