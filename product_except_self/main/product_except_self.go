package main

func productExceptSelf(nums []int) []int {
	n := len(nums)
	res := make([]int, n)

	// Calculate lefts and store in res.
	left := 1
	for i := 0; i < n; i++ {
		if i > 0 {
			left *= nums[i-1]
		}
		res[i] = left
	}

	// Calculate rights and product from the end of the array.
	right := 1
	for i := n-1; i >=0; i-- {
		if i < n-1 {
			right *= nums[i+1]
		}
		res[i] *= right
	}

	return res
}