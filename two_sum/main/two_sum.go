package main

import "github.com/davecgh/go-spew/spew"

func twoSum(nums []int, target int) []int {
	hash := make(map[int]int)

	for i, val := range nums {
		complement := target - val

		if idx, exists := hash[complement]; exists {
			return []int{idx, i}
		} else {
			hash[val] = i
		}
	}

	return nil
}

func main() {
	spew.Dump(twoSum([]int{2, 7, 11, 15}, 9))
}
