package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/path-sum-ii/
 * https://www.youtube.com/watch?v=zrN2dxtQ0f0&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=49&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func pathSum(root *TreeNode, sum int) [][]int {
	var ans [][]int
	var curr []int
	dfs(root, sum, &curr, &ans)
	return ans
}

func dfs(root *TreeNode, sum int, curr *[]int, ans *[][]int) {
	if root == nil {
		return
	}
	if root.Left == nil && root.Right == nil {
		if root.Val == sum {
			path := make([]int, len(*curr))
			copy(path, *curr)
			path = append(path, root.Val)
			*ans = append(*ans, path)
		}
	}

	*curr = append(*curr, root.Val)
	dfs(root.Left, sum-root.Val, curr, ans)
	dfs(root.Right, sum-root.Val, curr, ans)
	*curr = (*curr)[:len(*curr)-1]
}

func main() {
	root := &TreeNode{Val: 5}
	root.Left = &TreeNode{Val: 4}
	root.Right = &TreeNode{Val: 8}
	root.Left.Left = &TreeNode{Val: 11}
	root.Right.Left = &TreeNode{Val: 13}
	root.Right.Right = &TreeNode{Val: 4}
	root.Left.Left.Left = &TreeNode{Val: 7}
	root.Left.Left.Right = &TreeNode{Val: 2}
	root.Right.Right.Left = &TreeNode{Val: 5}
	root.Right.Right.Right = &TreeNode{Val: 1}

	spew.Dump(pathSum(root, 22))
}
