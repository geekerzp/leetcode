package main

func maxCount(m int, n int, ops [][]int) int {
	for _, op := range ops {
		m = min(m, op[0])
		n = min(n, op[1])
	}
	return m * n
}

func min(i, j int) int {
	if i < j {
		return i
	}
	return j
}
