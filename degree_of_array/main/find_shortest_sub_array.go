package main

func findShortestSubArray(nums []int) int {
	left, right, count := make(map[int]int), make(map[int]int), make(map[int]int)

	for i, x := range nums {
		if _, exists := left[x]; !exists {
			left[x] = i
		}
		right[x] = i
		count[x] += 1
	}

	var degree int
	for _, d := range count {
		if d > degree {
			degree = d
		}
	}

	ans := len(nums)
	for x, d := range count {
		if d == degree {
			if right[x]-left[x]+1 < ans {
				ans = right[x] - left[x] + 1
			}
		}
	}

	return ans
}

func main() {
	findShortestSubArray([]int{1, 2, 2, 3, 1})
}
