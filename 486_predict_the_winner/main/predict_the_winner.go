package main

func PredictTheWinner(nums []int) bool {
	memo := make([]int, len(nums)*len(nums))
	if getScore(memo, nums, 0, len(nums)-1) >= 0 {
		return true
	}
	return false
}

func getScore(memo []int, nums []int, l int, r int) int {
	if l == r {
		return nums[l]
	}
	k := len(nums)*l + r
	if memo[k] > 0 {
		return memo[k]
	}
	memo[k] = max(nums[l]-getScore(memo, nums, l+1, r), nums[r]-getScore(memo, nums, l, r-1))
	return memo[k] 
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
