package main

import "github.com/davecgh/go-spew/spew"

func rotate(matrix [][]int) {
	for i := 0; i < len(matrix)/2; i++ {
		j := len(matrix) - i - 1
		matrix[i], matrix[j] = matrix[j], matrix[i]
	}
	for i := 0; i < len(matrix); i++ {
		for j := i + 1; j < len(matrix); j++ {
			matrix[i][j], matrix[j][i] = matrix[j][i], matrix[i][j]
		}
	}
}

func main() {
	matrix := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	rotate(matrix)
	spew.Dump(matrix)
}
