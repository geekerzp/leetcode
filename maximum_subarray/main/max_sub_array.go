package main

func maxSubArray(nums []int) int {
	maxEndingHere, maxSoFar := nums[0], nums[0]
	for _, x := range nums[1:len(nums)] {
		maxEndingHere = max(x, maxEndingHere+x)
		maxSoFar = max(maxSoFar, maxEndingHere)
	}
	return maxSoFar
}

func max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}