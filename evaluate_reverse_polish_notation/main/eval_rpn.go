package main

import (
	"strconv"
)

func evalRPN(tokens []string) int {
	var stack []int
	for _, token := range tokens {
		if isOperand(token) {
			operand, _ := strconv.Atoi(token)
			stack = append([]int{operand}, stack...)
		} else if isOperator(token) {
			var operand1, operand2 int
			operand2, stack = stack[0], stack[1:len(stack)]
			operand1, stack = stack[0], stack[1:len(stack)]
			result := evaluate(operand1, operand2, token)
			stack = append([]int{result}, stack...)
		}
	}
	return stack[0]
}

func isOperator(token string) bool {
	if token == "+" || token == "-" || token == "*" || token == "/" {
		return true
	}
	return false
}

func isOperand(token string) bool {
	return !isOperator(token)
}

func evaluate(operand1, operand2 int, operator string) int {
	switch operator {
	case "+":
		return operand1 + operand2
	case "-":
		return operand1 - operand2
	case "*":
		return operand1 * operand2
	case "/":
		return operand1 / operand2
	}
	return 0
}

func main() {
	evalRPN([]string{"2", "1", "+", "3", "*"})
}
