package main

func doClimbStairs(i int, n int, memo []int) int {
	if i > n {
		return 0
	}
	if i == n {
		return 1
	}
	if memo[i] > 0 {
		return memo[i]
	}
	memo[i] = doClimbStairs(i+1, n, memo) + doClimbStairs(i+2, n, memo)
	return memo[i]
}

func climbStairs(n int) int {
	memo := make([]int, n+1)
	return doClimbStairs(0, n, memo)
}
