package main

import (
	"sort"

	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/combination-sum/
 * https://www.youtube.com/watch?v=zIY2BWdsbFs&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=34
 */

func combinationSum(candidates []int, target int) [][]int {
	var ans [][]int
	var cur []int
	sort.Ints(candidates)
	dfs(candidates, target, 0, &cur, &ans)
	return ans
}

func dfs(candidates []int, target int, s int, cur *[]int, ans *[][]int) {
	if target == 0 {
		cand := make([]int, len(*cur))
		copy(cand, *cur)
		*ans = append(*ans, cand)
		return
	}

	for i := s; i < len(candidates); i++ {
		if candidates[i] > target {
			break
		}
		*cur = append(*cur, candidates[i])
		dfs(candidates, target-candidates[i], i, cur, ans)
		*cur = (*cur)[:len(*cur)-1]
	}
}

func main() {
	spew.Dump(combinationSum([]int{2, 3, 6, 7}, 7))
}
