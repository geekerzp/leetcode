package main

type TrieNode struct {
	Links []*TrieNode
	R     int
	IsEnd bool
}

type Trie struct {
	Root *TrieNode
}

func (this *TrieNode) ContainsKey(ch rune) bool {
	return this.Links[ch-'a'] != nil
}

func (this *TrieNode) Get(ch rune) *TrieNode {
	return this.Links[ch-'a']
}

func (this *TrieNode) Put(ch rune, node *TrieNode) {
	this.Links[ch-'a'] = node
}

func (this *TrieNode) SetEnd() {
	this.IsEnd = true
}

func NewTrieNode() *TrieNode {
	return &TrieNode{R: 26, Links: make([]*TrieNode, 26)}
}

/** Initialize your data structure here. */
func Constructor() Trie {
	return Trie{Root: NewTrieNode()}
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	node := this.Root
	for _, currentChar := range word {
		if !node.ContainsKey(currentChar) {
			node.Put(currentChar, NewTrieNode())
		}
		node = node.Get(currentChar)
	}
	node.SetEnd()
}

// search a prefix or whole key in trie and
// returns the node where search ends
func (this *Trie) SearchPrefix(word string) *TrieNode {
	node := this.Root
	for _, currentLetter := range word {
		if node.ContainsKey(currentLetter) {
			node = node.Get(currentLetter)
		} else {
			return nil
		}
	}
	return node
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	node := this.SearchPrefix(word)
	return node != nil && node.IsEnd
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	node := this.SearchPrefix(prefix)
	return node != nil
}

func main() {
	trie := Constructor()
	trie.Insert("apple")
	trie.Search("apple")
	trie.Search("app")
	trie.StartsWith("app")
}
