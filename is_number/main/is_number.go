package main

func isNumber(s string) bool {
	digits := map[rune]bool{
		'0': true, '1': true, '2': true, '3': true, '4': true,
		'5': true, '6': true, '7': true, '8': true, '9': true,
	}

	blanks := map[rune]bool{
		' ': true, '\t': true, '\n': true,
	}

	signs := map[rune]bool{
		'+': true, '-': true,
	}

	e := 'e'

	dot := '.'

	states := []map[string]int{
		// State (0) - initial state ('', ' ')
		{"blank": 0, "sign": 1, "digit": 2, ".": 3},
		// State (1) - found sign ('+', '-')
		{"digit": 2, ".": 3},
		// State (2) - digit consumer ('+3', '-3', '33')
		{"digit": 2, ".": 4, "e": 5, "blank": 8},
		// State (3) - found dot ('.', '+.', '-.')
		{"digit": 4},
		// State (4) - after dot ('3.', '3.1', '3.12')
		{"digit": 4, "e": 5, "blank": 8},
		// State (5) - found 'e' ('3e', '3.12e')
		{"sign": 6, "digit": 7},
		// State (6) - sign after 'e' ('3.12e+')
		{"digit": 7},
		// State (7) - digit after 'e' ('3.12e2', '3.12e+2', '3.12e+24')
		{"digit": 7, "blank": 8},
		// State (8) - Terminal state
		{"blank": 8},
	}

	currentState := 0
	runeType := ""

	for _, c := range s {
		if digits[c] {
			runeType = "digit"
		} else if blanks[c] {
			runeType = "blank"
		} else if signs[c] {
			runeType = "sign"
		} else if c == e {
			runeType = "e"
		} else if c == dot {
			runeType = "."
		} else {
			return false
		}

		if _, exists := states[currentState][runeType]; !exists {
			return false
		}

		currentState = states[currentState][runeType]
	}

	if currentState != 2 && currentState != 4 && currentState != 7 && currentState != 8 {
		return false
	}

	return true
}