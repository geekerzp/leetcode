package main

import "sort"

func groupAnagrams(strs []string) [][]string {
	ans := make(map[string][]string)

	for _, s := range strs {
		ca := []rune(s)
		sort.Slice(ca, func(i, j int) bool {
			return ca[i] < ca[j]
		})
		key := string(ca)
		println(key)
		if val, exists := ans[key]; exists {
			ans[key] = append(val, s)
		} else {
			ans[key] = []string{s}
		}
	}

	var res [][]string
	for _, val := range ans {
		res = append(res, val)
	}

	return res
}

func main() {
	println(groupAnagrams([]string{"eat","tea","tan","ate","nat","bat"}))
}
