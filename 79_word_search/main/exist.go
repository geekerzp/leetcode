package main

func exist(board [][]byte, word string) bool {
	visited := make([][]bool, len(board))
	for i := 0; i < len(board); i++ {
		visited[i] = make([]bool, len(board[i]))
	}

	for i := 0; i < len(board); i++ {
		for j := 0; j < len(board[i]); j++ {
			if word[0] == board[i][j] && search(board, word, visited, i, j, 0) {
				return true
			}
		}
	}
	return false
}

func search(board [][]byte, word string, visited [][]bool, i int, j int, index int) bool {
	if index == len(word) {
		return true
	}

	if i >= len(board) || i < 0 || j >= len(board[i]) || j < 0 || board[i][j] != word[index] || visited[i][j] {

		return false
	}

	visited[i][j] = true

	if search(board, word, visited, i-1, j, index+1) ||
		search(board, word, visited, i+1, j, index+1) ||
		search(board, word, visited, i, j-1, index+1) ||
		search(board, word, visited, i, j+1, index+1) {
		return true
	}

	visited[i][j] = false
	return false
}
