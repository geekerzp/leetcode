package main

/**
 * https://leetcode.com/problems/pacific-atlantic-water-flow/
 * https://www.youtube.com/watch?v=zV3o4XVoU8M&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=8
 */

func pacificAtlantic(matrix [][]int) [][]int {
	if len(matrix) == 0 {
		return nil
	}
	n := len(matrix)
	m := len(matrix[0])

	p := make([][]int, n)
	for i := range p {
		p[i] = make([]int, m)
	}
	a := make([][]int, n)
	for i := range a {
		a[i] = make([]int, m)
	}

	for x := 0; x < m; x++ {
		dfs(matrix, x, 0, 0, &p)   // top
		dfs(matrix, x, n-1, 0, &a) // bottom
	}

	for y := 0; y < n; y++ {
		dfs(matrix, 0, y, 0, &p)   // left
		dfs(matrix, m-1, y, 0, &a) // right
	}

	var ans [][]int
	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			if p[i][j] == 1 && a[i][j] == 1 {
				ans = append(ans, []int{i, j})
			}
		}
	}

	return ans
}

func dfs(b [][]int, x int, y int, h int, v *[][]int) {
	if x < 0 || y < 0 || x == len(b[0]) || y == len(b) {
		return
	}
	if (*v)[y][x] == 1 || b[y][x] < h {
		return
	}
	(*v)[y][x] = 1
	dfs(b, x+1, y, b[y][x], v)
	dfs(b, x-1, y, b[y][x], v)
	dfs(b, x, y+1, b[y][x], v)
	dfs(b, x, y-1, b[y][x], v)
}

func main() {

}
