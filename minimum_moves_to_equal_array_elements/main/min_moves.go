package main

import "math"

func minMoves(nums []int) int {
	sum := 0
	for _, n := range nums {
		sum += n
	}

	minNum := math.MaxInt64
	for _, n := range nums {
		if n < minNum {
			minNum = n
		}
	}

	return sum - minNum*len(nums)
}
