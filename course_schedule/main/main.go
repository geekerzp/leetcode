package main

func canFinish(numCourses int, prerequisites [][]int) bool {
	adj := make(map[int][]int)
	for e := 0; e < len(prerequisites); e++ {
		adj[prerequisites[e][1]] = append(adj[prerequisites[e][1]], prerequisites[e][0])
	}
	isVisited := make([]bool, numCourses)
	for v := 0; v < numCourses; v++ {
		if !isVisited[v] {
			onStack := make([]bool, numCourses)
			if hasCycle(adj, v, &isVisited, &onStack) {
				return false
			}
		}
	}

	return true
}

func hasCycle(adj map[int][]int, v int, isVisited *[]bool, onStack *[]bool) bool {
	(*isVisited)[v] = true
	(*onStack)[v] = true

	for _, w := range adj[v] {
		if (*onStack)[w] {
			return true
		} else if !(*isVisited)[w] {
			if hasCycle(adj, w, isVisited, onStack) {
				return true
			}
		}
	}

	(*onStack)[v] = false
	return false
}

func main() {
	canFinish(2, [][]int{{1, 0}})
}
