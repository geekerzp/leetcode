package main

import "github.com/davecgh/go-spew/spew"

/**
 * https://leetcode.com/problems/open-the-lock/
 * https://www.youtube.com/watch?v=oUeGFKZvoo4&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN
 */

func openLock(deadends []string, target string) int {
	start := "0000"
	deadendsMap := make(map[string]bool)
	for _, e := range deadends {
		deadendsMap[e] = true
	}
	if _, exists := deadendsMap[start]; exists {
		return -1
	}

	var steps int
	q := []string{start}
	visitedMap := make(map[string]bool)
	for len(q) > 0 {
		steps++
		size := len(q)
		for i := 0; i < size; i++ {
			var curr string
			curr, q = q[0], q[1:]
			for j := 0; j < 4; j++ {
				for _, v := range []int{-1, 1} {
					b := []byte(curr)
					b[j] = intToByte((byteToInt(b[j]) + v + 10) % 10)
					next := string(b)
					if next == target {
						return steps
					}
					if deadendsMap[next] || visitedMap[next] {
						continue
					}
					q = append(q, next)
					visitedMap[next] = true
				}
			}
		}
	}

	return -1
}

func byteToInt(b byte) int {
	return int(b - '0')
}

func intToByte(i int) byte {
	return byte(i + '0')
}

func main() {
	ans := openLock([]string{"0201", "0101", "0102", "1212", "2002"}, "0202")
	spew.Dump(ans)
}
