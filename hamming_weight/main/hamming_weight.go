package main

func hammingWeight(n int) int {
	bits, mask := 0, 1
	for i := 0; i < 32; i++ {
		if (n & mask) != 0 {
			bits++
		}
		mask <<= 1
	}
	return bits
}

func main() {
	println(hammingWeight(2))
}
