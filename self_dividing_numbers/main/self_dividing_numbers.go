package main

func selfDividingNumbers(left int, right int) []int {
	var slice []int

	for i := left; i <= right; i++ {
		j := i

		for ; j > 0; j /= 10 {
			if (j % 10 == 0) || (i % (j % 10) != 0) {
				break
			}
		}

		if j == 0 {
			slice = append(slice, i);
		}
	}

	return slice
}
