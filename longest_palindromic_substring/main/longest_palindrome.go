package main

func longestPalindrome(s string) string {
	start, end := 0, 0
	for i := 0; i < len(s); i++ {
		len1 := expendAroundCenter(s, i, i)
		len2 := expendAroundCenter(s, i, i+1)
		maxlen := max(len1, len2)
		if maxlen > end-start {
			start = i - (maxlen-1)/2
			end = i + maxlen/2
		}
	}
	return s[start : end+1]
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func expendAroundCenter(s string, left, right int) int {
	L, R := left, right
	for L >= 0 && R < len(s) && s[L] == s[R] {
		L--
		R++
	}
	return R - L - 1
}
