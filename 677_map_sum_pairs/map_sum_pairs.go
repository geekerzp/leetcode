package main

type Trie struct {
	Sum      int
	Children map[rune]*Trie
}

type MapSum struct {
	Root *Trie
	Vals map[string]int
}

/** Initialize your data structure here. */
func Constructor() MapSum {
	return MapSum{
		Root: &Trie{Children: make(map[rune]*Trie)},
		Vals: make(map[string]int),
	}
}

func (m *MapSum) Insert(key string, val int) {
	inc := val - m.Vals[key]
	p := m.Root
	for _, c := range key {
		if _, exists := p.Children[c]; !exists {
			p.Children[c] = &Trie{Children: make(map[rune]*Trie)}
		}
		p.Children[c].Sum += inc
		p = p.Children[c]
	}
	m.Vals[key] = val
}

func (m *MapSum) Sum(prefix string) int {
	p := m.Root
	for _, c := range prefix {
		if _, exists := p.Children[c]; !exists {
			return 0
		}
		p = p.Children[c]
	}
	return p.Sum
}

/**
 * Your MapSum object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(key,val);
 * param_2 := obj.Sum(prefix);
 */

func main() {

}
