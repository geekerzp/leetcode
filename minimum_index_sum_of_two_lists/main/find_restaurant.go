package main

import (
	"math"
)

func findRestaurant(list1 []string, list2 []string) []string {
	sumMap := make(map[int][]string)
	for idx1, val1 := range list1 {
		for idx2, val2 := range list2 {
			if val1 == val2 {
				sumMap[idx1+idx2] = append(sumMap[idx1+idx2], val1)
			}
		}
	}

	minIndexSum := math.MaxInt64
	for key := range sumMap {
		if key < minIndexSum {
			minIndexSum = key
		}
	}
	return sumMap[minIndexSum]
}

func main() {
	findRestaurant([]string{"Shogun", "Tapioca Express", "Burger King", "KFC"}, []string{"Piatti", "The Grill at Torrey Pines", "Hungry Hunter Steakhouse", "Shogun"})
}
