package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/redundant-connection-ii/
 * https://www.youtube.com/watch?v=lnmJT5b4NlM&list=PLLuMmzMTgVK5gFVMpryw0LkJp4l9WTtdM&index=37&t=0s
 */

func findRedundantDirectedConnection(edges [][]int) []int {
	parents := make([]int, len(edges)+1)
	up := make([]int, len(edges)+1)
	rank := make([]int, len(edges)+1)

	var ans1 []int
	var ans2 []int

	for _, edge := range edges {
		u, v := edge[0], edge[1]

		// A node has two parents
		if parents[v] > 0 {
			ans1 = []int{parents[v], v}
			ans2 = append([]int{}, edge...)

			// Delete the later edge
			edge[0], edge[1] = -1, -1
		}

		parents[v] = u
	}

	// spew.Dump(parents)
	// spew.Dump(ans1)
	// spew.Dump(ans2)

	for _, edge := range edges {
		u, v := edge[0], edge[1]

		// Invalid edge (we deleted in step 1)
		if u < 0 && v < 0 {
			continue
		}

		if !union(up, rank, u, v) {
			if len(ans1) == 0 {
				return edge
			} else {
				return ans1
			}
		}
	}

	return ans2
}

func find(up []int, x int) int {
	if up[x] == 0 {
		return x
	} else {
		up[x] = find(up, up[x])
		return up[x]
	}
}

func union(up []int, rank []int, x int, y int) bool {
	reprX := find(up, x)
	reprY := find(up, y)
	if reprX == reprY {
		return false
	}
	if rank[reprX] == rank[reprY] {
		rank[reprX]++
		up[reprY] = reprX
	} else if rank[reprX] > rank[reprY] {
		up[reprY] = reprX
	} else {
		up[reprX] = reprY
	}
	return true
}

func main() {
	ans := findRedundantDirectedConnection([][]int{{1, 2}, {2, 3}, {3, 4}, {4, 1}, {1, 5}})
	spew.Dump(ans)
}
