package main

// TreeNode ...
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func diameterOfBinaryTree(root *TreeNode) int {
	ans := 1
	dfs(root, &ans)
	return ans - 1
}

func dfs(node *TreeNode, ans *int) int {
	if node == nil {
		return 0
	}
	L := dfs(node.Left, ans)
	R := dfs(node.Right, ans)
	*ans = max(*ans, L+R+1)
	return max(L, R) + 1
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {

}
