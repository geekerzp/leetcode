package main

func imageSmoother(M [][]int) [][]int {
	R, C := len(M), len(M[0])
	ans := make([][]int, R)
	for r := 0; r < R; r++ {
		ans[r] = make([]int, C)
	}

	for r := 0; r < R; r++ {
		for c := 0; c < C; c++ {
			var count int
			for nr := r - 1; nr <= r+1; nr++ {
				for nc := c - 1; nc <= c+1; nc++ {
					if 0 <= nr && nr < R && 0 <= nc && nc < C {
						ans[r][c] += M[nr][nc]
						count++
					}
				}
			}
			ans[r][c] /= count
		}
	}

	return ans
}

func main() {
	imageSmoother([][]int{{1, 1, 1}, {1, 0, 1}, {1, 1, 1}})
}
