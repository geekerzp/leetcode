package main

func strStr(haystack string, needle string) int {
	l1, l2 := len(haystack), len(needle)
	if l2 == 0 {
		return 0
	}
	if l1 < l2 {
		return -1
	}
	threshold := l1 - l2
	for i := 0; i <= threshold; i++ {
		if haystack[i:i+l2] == needle {
			return i
		}
	}
	return -1
}

func main() {
	strStr("hello", "ll")
}