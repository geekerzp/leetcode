package main

/**
 * https://leetcode.com/problems/keys-and-rooms/
 * https://www.youtube.com/watch?v=y46WQ5KMiD8&list=PLLuMmzMTgVK5gFVMpryw0LkJp4l9WTtdM&index=22&t=0s
 */

func canVisitAllRooms(rooms [][]int) bool {
	marked := make([]bool, len(rooms))
	dfs(rooms, 0, marked)
	for _, v := range marked {
		if !v {
			return false
		}
	}
	return true
}

func dfs(graph [][]int, v int, marked []bool) {
	marked[v] = true
	for _, n := range graph[v] {
		if !marked[n] {
			dfs(graph, n, marked)
		}
	}
}

func main() {
	ans := canVisitAllRooms([][]int{{1}, {2}, {3}, {}})
	println(ans)
}
