package main

import "github.com/davecgh/go-spew/spew"

func findOrder(numCourses int, prerequisites [][]int) []int {
	var reversePostOrder []int
	adj := make(map[int][]int)
	for e := 0; e < len(prerequisites); e++ {
		adj[prerequisites[e][1]] = append(adj[prerequisites[e][1]], prerequisites[e][0])
	}
	isVisited := make([]bool, numCourses)
	for v := 0; v < numCourses; v++ {
		if !isVisited[v] {
			onStack := make([]bool, numCourses)
			if hasCycle(adj, v, &isVisited, &onStack, &reversePostOrder) {
				return nil
			}
		}
	}
	return reversePostOrder
}

func hasCycle(adj map[int][]int, v int, isVisited *[]bool, onStack *[]bool, reversePostOrder *[]int) bool {
	(*isVisited)[v] = true
	(*onStack)[v] = true

	for _, w := range adj[v] {
		if (*onStack)[w] {
			return true
		} else if !(*isVisited)[w] {
			if hasCycle(adj, w, isVisited, onStack, reversePostOrder) {
				return true
			}
		}
	}

	(*onStack)[v] = false
	*reversePostOrder = append([]int{v}, *reversePostOrder...)
	return false
}

func main() {
	spew.Dump(findOrder(4, [][]int{{1, 0}, {2, 0}, {3, 1}, {3, 2}}))
}
