package main

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

var leftCounts, rightCounts int

func btreeGameWinningMove(root *TreeNode, n int, x int) bool {
	nodes(root, x)
	parentCounts := n - (leftCounts + rightCounts + 1)
	return max(parentCounts, max(leftCounts, rightCounts)) > n/2
}

func nodes(root *TreeNode, x int) int {
	if root == nil {
		return 0
	}
	l := nodes(root.Left, x)
	r := nodes(root.Right, x)
	if root.Val == x {
		leftCounts = l
		rightCounts = r
	}
	return 1 + l + r
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}
