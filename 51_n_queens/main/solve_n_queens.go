package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/n-queens/
 * https://www.youtube.com/watch?v=Xa-yETqFNEQ&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=40
 */

type Board struct {
	n     int
	cols  map[int]bool
	diag1 map[int]bool
	diag2 map[int]bool
	board [][]byte
}

func (b *Board) available(x int, y int) bool {
	return !b.cols[x] && !b.diag1[x+y] && !b.diag2[x-y+b.n-1]
}

func (b *Board) put(x int, y int) {
	b.cols[x] = true
	b.diag1[x+y] = true
	b.diag2[x-y+b.n-1] = true
	b.board[y][x] = 'Q'
}

func (b *Board) remove(x int, y int) {
	b.cols[x] = false
	b.diag1[x+y] = false
	b.diag2[x-y+b.n-1] = false
	b.board[y][x] = '.'
}

func (b *Board) getBoard() []string {
	var board []string
	for _, row := range b.board {
		board = append(board, string(row))
	}
	return board
}

func (b *Board) nqueens(y int, ans *[][]string) {
	if y == b.n {
		*ans = append(*ans, b.getBoard())
		return
	}

	for x := 0; x < b.n; x++ {
		if !b.available(x, y) {
			continue
		}
		b.put(x, y)
		b.nqueens(y+1, ans)
		b.remove(x, y)
	}
}

func NewBoard(n int) *Board {
	b := new(Board)
	b.n = n
	b.cols = make(map[int]bool)
	b.diag1 = make(map[int]bool)
	b.diag2 = make(map[int]bool)
	b.board = make([][]byte, n)
	for i := range b.board {
		b.board[i] = make([]byte, n)
		for j := range b.board[i] {
			b.board[i][j] = '.'
		}
	}
	return b
}

func solveNQueens(n int) [][]string {
	b := NewBoard(n)
	var ans [][]string
	b.nqueens(0, &ans)
	return ans
}

func main() {
	spew.Dump(solveNQueens(4))
}
