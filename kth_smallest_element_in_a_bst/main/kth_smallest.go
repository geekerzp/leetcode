package main

// TreeNode for internal
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func countNodes(n *TreeNode) int {
	if n == nil {
		return 0
	}

	return 1 + countNodes(n.Left) + countNodes(n.Right)
}

func kthSmallest(root *TreeNode, k int) int {
	count := countNodes(root.Left)
	if k <= count {
		return kthSmallest(root.Left, k)
	} else if k > count+1 {
		return kthSmallest(root.Right, k-count-1)
	}

	return root.Val
}

func main() {
	root := &TreeNode{Val: 3}
	root.Left = &TreeNode{Val: 1}
	root.Right = &TreeNode{Val: 4}
	root.Left.Right = &TreeNode{Val: 2}

	println(kthSmallest(root, 1))
}
