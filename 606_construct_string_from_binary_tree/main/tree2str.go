package main

import (
	"strconv"
)

/**
 * https://leetcode.com/problems/construct-string-from-binary-tree/
 * https://www.youtube.com/watch?v=EggWOgUnt2M&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=50&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func tree2str(t *TreeNode) string {
	if t == nil {
		return ""
	}
	s := strconv.Itoa(t.Val)
	l := tree2str(t.Left)
	r := tree2str(t.Right)

	if t.Left == nil && t.Right == nil {
		return s
	}
	if t.Right == nil {
		return s + "(" + l + ")"
	}
	return s + "(" + l + ")" + "(" + r + ")"
}

func main() {

}
