package main

func rob(nums []int) int {
	a, b := 0, 0
	for i, val := range nums {
		if i%2 == 0 {
			if val+a > b {
				a += val
			} else {
				a = b
			}
		} else {
			if val+b > a {
				b += val
			} else {
				b = a
			}
		}
	}

	if a > b {
		return a
	} else {
		return b
	}
}
