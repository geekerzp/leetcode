package main

// https://leetcode.com/problems/design-hashmap/description/

type BucketNode struct {
	Key  int
	Val  int
	Next *BucketNode
}

type Bucket struct {
	Head *BucketNode
}

func NewBucket() *Bucket {
	return &Bucket{Head: &BucketNode{Key: -1, Val: -1}}
}

type MyHashMap struct {
	Buckets []*Bucket
}

func Constructor() MyHashMap {
	return MyHashMap{Buckets: make([]*Bucket, 10000)}
}

func (myHashMap *MyHashMap) Find(bucket *Bucket, key int) *BucketNode {
	node := bucket.Head
	var prev *BucketNode
	for node != nil && node.Key != key {
		prev = node
		node = node.Next
	}
	return prev
}

func (myHashMap *MyHashMap) Put(key int, value int) {
	i := key % len(myHashMap.Buckets)
	if myHashMap.Buckets[i] == nil {
		myHashMap.Buckets[i] = NewBucket()
	}
	prev := myHashMap.Find(myHashMap.Buckets[i], key)
	if prev.Next == nil {
		prev.Next = &BucketNode{Key: key, Val: value}
	} else {
		prev.Next.Val = value
	}
}

func (myHashMap *MyHashMap) Get(key int) int {
	i := key % len(myHashMap.Buckets)
	if myHashMap.Buckets[i] == nil {
		return -1
	}
	node := myHashMap.Find(myHashMap.Buckets[i], key)
	if node.Next == nil {
		return -1
	} else {
		return node.Next.Val
	}
}

func (myHashMap *MyHashMap) Remove(key int) {
	i := key % len(myHashMap.Buckets)
	if myHashMap.Buckets[i] == nil {
		return
	}
	prev := myHashMap.Find(myHashMap.Buckets[i], key)
	if prev.Next == nil {
		return
	}
	prev.Next = prev.Next.Next
}
