package main

import (
	"container/heap"
)

type Item struct {
	value    interface{}
	priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int {
	return len(pq)
}

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = j
	pq[j].index = i
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1
	*pq = old[0 : n-1]
	return item
}

func (pq *PriorityQueue) update(item *Item, value interface{}, priority int) {
	item.value = value
	item.priority = priority
	heap.Fix(pq, item.index)
}

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeKLists(lists []*ListNode) *ListNode {
	head := &ListNode{Val: 0}
	point := head
	pq := PriorityQueue{}
	heap.Init(&pq)

	for _, l := range lists {
		if l != nil {
			heap.Push(&pq, &Item{value: l, priority: l.Val})
		}
	}

	for pq.Len() != 0 {
		node := heap.Pop(&pq).(*Item).value.(*ListNode)
		point.Next = &ListNode{Val: node.Val}
		point = point.Next
		node = node.Next
		if node != nil {
			heap.Push(&pq, &Item{value: node, priority: node.Val})
		}
	}

	return head.Next
}

func main() {
}
