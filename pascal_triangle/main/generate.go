package main

func generate(numRows int) [][]int {
	var triangle [][]int

	if numRows == 0 {
		return triangle
	}

	triangle = append(triangle, []int{1})

	for rowNum := 1; rowNum < numRows; rowNum++ {
		var row []int
		var preRow = triangle[rowNum-1]

		row = append(row, 1)

		for j := 1; j < rowNum; j++ {
			row = append(row, preRow[j-1] + preRow[j])
		}

		row = append(row, 1)

		triangle = append(triangle, row)
	}

	return triangle
}
