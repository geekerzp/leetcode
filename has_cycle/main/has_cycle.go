package main

type ListNode struct {
	Val int
	Next *ListNode
}

func hasCycle(head *ListNode) bool {
	nodesSeen := make(map[*ListNode]bool)

	for head != nil {
		if nodesSeen[head] {
			return true
		} else {
			nodesSeen[head] = true
		}
	}

	return false
}
