package main

import "github.com/davecgh/go-spew/spew"

func sortColor(nums []int) {
	red := 0
	blue := len(nums) - 1

	for i := 0; i <= blue; i++ {
		if nums[i] == 0 {
			nums[i], nums[red] = nums[red], nums[i]
			red++
		} else if nums[i] == 2 {
			nums[i], nums[blue] = nums[blue], nums[i]
			i--
			blue--
		}
	}
}

func main() {
	nums := []int{2, 0, 2, 1, 1, 0}
	sortColor(nums)
	spew.Dump(nums)
}
