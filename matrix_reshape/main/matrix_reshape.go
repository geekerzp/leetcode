package main

func matrixReshape(nums [][]int, r int, c int) [][]int {
	res := make([][]int, r)
	for i := range res {
		res[i] = make([]int, c)
	}

	if len(nums) == 0 || r*c != len(nums)*len(nums[0]) {
		return nums
	}

	var queue []int
	for i := 0; i < len(nums); i++ {
		for j := 0; j < len(nums[i]); j++ {
			queue = append(queue, nums[i][j])
		}
	}

	for i := 0; i < r; i++ {
		for j := 0; j < c; j++ {
			var num int
			num, queue = queue[0], queue[1:]
			res[i][j] = num
		}
	}

	return res
}


