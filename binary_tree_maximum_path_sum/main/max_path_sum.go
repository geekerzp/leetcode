package main

import "math"

/*
 * https://leetcode.com/problems/binary-tree-maximum-path-sum/
 */

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func maxPathSum(root *TreeNode) int {
	if root == nil {
		return 0
	}
	ans := math.MinInt64
	doMaxPathSum(root, &ans)
	return ans
}

func doMaxPathSum(root *TreeNode, ans *int) int {
	if root == nil {
		return 0
	}
	l := max(0, doMaxPathSum(root.Left, ans))
	r := max(0, doMaxPathSum(root.Right, ans))
	sum := l + r + root.Val
	*ans = max(*ans, sum)
	return max(l, r) + root.Val
}

func max(i int, j int) int {
	if i > j {
		return i
	}
	return j
}
