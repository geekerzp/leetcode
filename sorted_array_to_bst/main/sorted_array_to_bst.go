package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func constructNode(nums []int, low int, high int) *TreeNode {
	if low > high {
		return nil
	}

	mid := (low + high) / 2
	node := &TreeNode{Val: nums[mid]}
	node.Left = constructNode(nums, low, mid-1)
	node.Right = constructNode(nums, mid+1, high)
	return node
}

func sortedArrayToBST(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	head := constructNode(nums, 0, len(nums)-1)
	return head
}
