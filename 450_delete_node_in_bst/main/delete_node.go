package main

/**
 * https://leetcode.com/problems/delete-node-in-a-bst/
 * https://www.youtube.com/watch?v=00r9qf7lgAk&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=16&t=0s
 */

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deleteNode(root *TreeNode, key int) *TreeNode {
	if root == nil {
		return root
	}
	if key > root.Val {
		root.Right = deleteNode(root.Right, key)
	} else if key < root.Val {
		root.Left = deleteNode(root.Left, key)
	} else {
		if root.Left != nil && root.Right != nil {
			min := root.Right
			for min.Left != nil {
				min = min.Left
			}
			root.Val = min.Val
			root.Right = deleteNode(root.Right, min.Val)
		} else {
			var newRoot *TreeNode
			if root.Left == nil {
				newRoot = root.Right
			}
			if root.Right == nil {
				newRoot = root.Left
			}
			return newRoot
		}
	}
	return root
}

func main() {}
