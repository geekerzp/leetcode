package main

import (
	"math"
)

/**
 * https://leetcode.com/problems/my-calendar-iii/
 * https://www.youtube.com/watch?v=yK9a-rT3FBQ&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=23&t=0s
 */

type Node struct {
	L     int
	M     int
	R     int
	Count int
	Left  *Node
	Right *Node
}

type MyCalendarThree struct {
	Root     *Node
	MaxCount int
}

func NewNode(l int, r int, count int) *Node {
	return &Node{L: l, M: -1, R: r, Count: count}
}

func (c *MyCalendarThree) Add(start int, end int, root *Node) {
	if root.M != -1 {
		if end <= root.M {
			c.Add(start, end, root.Left)
		} else if start >= root.M {
			c.Add(start, end, root.Right)
		} else {
			c.Add(start, root.M, root.Left)
			c.Add(root.M, end, root.Right)
		}
		return
	}

	if start == root.L && end == root.R {
		root.Count += 1
		c.MaxCount = maxInt(c.MaxCount, root.Count)
	} else if start == root.L {
		root.M = end
		root.Left = NewNode(start, root.M, root.Count+1)
		root.Right = NewNode(root.M, root.R, root.Count)
		c.MaxCount = maxInt(c.MaxCount, root.Count+1)
	} else if end == root.R {
		root.M = start
		root.Left = NewNode(root.L, root.M, root.Count)
		root.Right = NewNode(root.M, root.R, root.Count+1)
		c.MaxCount = maxInt(c.MaxCount, root.Count+1)
	} else {
		root.M = start
		root.Left = NewNode(root.L, root.M, root.Count)
		root.Right = NewNode(root.M, root.R, root.Count)
		c.Add(start, end, root.Right)
	}
}

func Constructor() MyCalendarThree {
	return MyCalendarThree{Root: NewNode(0, math.MaxInt64, 0)}
}

func (c *MyCalendarThree) Book(start int, end int) int {
	c.Add(start, end, c.Root)
	return c.MaxCount
}

func maxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

/**
 * Your MyCalendarThree object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Book(start,end);
 */

func main() {
	c := Constructor()
	println(c.Book(10, 20))
	println(c.Book(50, 60))
	println(c.Book(10, 40))
	println(c.Book(5, 15))
	println(c.Book(5, 10))
	println(c.Book(25, 55))
}
