package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func findTilt(root *TreeNode) int {
	tilt := 0
	traverse(root, &tilt)
	return tilt
}

func traverse(root *TreeNode, tilt *int) int {
	if root == nil {
		return 0
	}
	left := traverse(root.Left, tilt)
	right := traverse(root.Right, tilt)
	*tilt += abs(left - right)
	return left + right + root.Val
}

func abs(n int) int {
	y := n >> 63
	return (n ^ y) - y
}
