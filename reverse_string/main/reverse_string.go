package main

func reverseString(s string) string {
	word := []byte(s)
	length := len(s)

	for i := 0; i < length / 2; i++ {
		temp := word[i]
		word[i] = word[length - 1 - i]
		word[length - 1 - i] = temp
	}

	return string(word)
}


