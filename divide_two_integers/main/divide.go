package main

import (
	"math"
)

func divide(dividend int, divisor int) int {
	if (divisor == 0) || (dividend == math.MinInt32 && divisor == -1) {
		return math.MaxInt32
	}

	var sign int
	if (dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0) {
		sign = -1
	} else {
		sign = 1
	}

	dvd := abs(dividend)
	dvs := abs(divisor)
	var res int
	for dvd >= dvs {
		temp, multiple := dvs, 1
		for dvd >= (temp << 1) {
			temp <<= 1
			multiple <<= 1
		}
		dvd -= temp
		res += multiple
	}

	if sign == 1 {
		return res
	} else {
		return -res
	}
}

func abs(x int) int {
	if x > 0 {
		return x
	}
	return -x
}

func main() {
}
