package main

import (
	"sort"

	"github.com/davecgh/go-spew/spew"
)

func subsets(nums []int) [][]int {
	list := [][]int{}
	sort.Ints(nums)
	backtrack(&list, &[]int{}, nums, 0)
	return list
}

func backtrack(list *[][]int, tempList *[]int, nums []int, start int) {
	*list = append(*list, append([]int{}, *tempList...))
	for i := start; i < len(nums); i++ {
		*tempList = append(*tempList, nums[i])
		backtrack(list, tempList, nums, i+1)
		*tempList = (*tempList)[:len(*tempList)-1]
	}
}

func main() {
	spew.Dump(subsets([]int{1, 2, 3}))
}
