package main

/**
 * https://leetcode.com/problems/shortest-bridge/
 * https://www.youtube.com/watch?v=JU-g1mNUaSE&list=PLLuMmzMTgVK5gFVMpryw0LkJp4l9WTtdM&index=15
 */

func shortestBridge(A [][]int) int {
	var q [][]int
	var found bool
	for i := 0; i < len(A) && !found; i++ {
		for j := 0; j < len(A[0]) && !found; j++ {
			if A[i][j] == 1 {
				dfs(A, j, i, &q)
				found = true
			}
		}
	}

	var steps int
	dirs := []int{0, 1, 0, -1, 0}
	for len(q) > 0 {
		size := len(q)
		for size > 0 {
			x := q[0][0]
			y := q[0][1]
			q = q[1:]
			for i := 0; i < 4; i++ {
				tx := x + dirs[i]
				ty := y + dirs[i+1]
				if tx < 0 || ty < 0 || tx >= len(A[0]) || ty >= len(A) || A[ty][tx] == 2 {
					continue
				}
				if A[ty][tx] == 1 {
					return steps
				}
				A[ty][tx] = 2
				q = append(q, []int{tx, ty})
			}
			size--
		}
		steps++
	}
	return -1
}

func dfs(A [][]int, x int, y int, q *[][]int) {
	if x < 0 || y < 0 || x >= len(A[0]) || y >= len(A) || A[y][x] != 1 {
		return
	}
	A[y][x] = 2
	*q = append(*q, []int{x, y})
	dfs(A, x-1, y, q)
	dfs(A, x, y-1, q)
	dfs(A, x+1, y, q)
	dfs(A, x, y+1, q)
}

func main() {
	ans := shortestBridge([][]int{{0, 1, 0}, {0, 0, 0}, {0, 0, 1}})
	println(ans)
}
