package main

import (
	"strconv"
	"sort"
	"strings"
)

func largestNumber(nums []int) string {
	asStrs := make([]string, len(nums))
	for i, num := range nums {
		asStrs[i] = strconv.Itoa(num)
	}

	sort.Slice(asStrs, func(i, j int) bool {
		order1 := asStrs[i] + asStrs[j]
		order2 := asStrs[j] + asStrs[i]
		return order1 > order2
	})

	if asStrs[0] == "0" {
		return "0"
	}

	return strings.Join(asStrs, "")
}
