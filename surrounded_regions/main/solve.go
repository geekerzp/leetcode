package main

func solve(board [][]byte) {
	if board == nil || len(board) == 0 {
		return
	}
	rows, cols := len(board), len(board[0])

	// check first and last col
	for i := 0; i < rows; i++ {
		if board[i][0] == 'O' {
			dfs(i, 1, board)
		}
		if board[i][cols-1] == 'O' {
			dfs(i, cols-2, board)
		}
	}

	// check first row and last row
	for i := 0; i < cols; i++ {
		if board[0][i] == 'O' {
			dfs(1, i, board)
		}
		if board[rows-1][i] == 'O' {
			dfs(rows-2, i, board)
		}
	}

	// flip O to X, '*' to 'O'
	// skip the boulders
	for i := 1; i < rows-1; i++ {
		for j := 1; j < cols-1; j++ {
			if board[i][j] == '*' {
				board[i][j] = 'O'
			} else if board[i][j] == 'O' {
				board[i][j] = 'X'
			}
		}
	}
}

func dfs(i int, j int, board [][]byte) {
	if i <= 0 || i >= len(board)-1 || j <= 0 || j >= len(board[0])-1 || board[i][j] == 'X' {
		return
	}
	if board[i][j] == '*' {
		return
	}
	if board[i][j] == 'O' {
		board[i][j] = '*'
	}

	dfs(i+1, j, board)
	dfs(i-1, j, board)
	dfs(i, j+1, board)
	dfs(i, j-1, board)
}

func main() {

}
