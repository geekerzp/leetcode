package main

func romanToInt(s string) int {
	T := map[byte]int{
		'I': 1,
		'V': 5,
		'X': 10,
		'L': 50,
		'C': 100,
		'D': 500,
		'M': 1000,
	}

	sum := T[s[len(s) - 1]]
	for i := len(s) - 2; i >= 0; i-- {
		if T[s[i]] < T[s[i + 1]] {
			sum -= T[s[i]]
		} else {
			sum += T[s[i]]
		}
	}

	return sum
}
