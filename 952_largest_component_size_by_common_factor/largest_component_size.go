package main

import (
	"math"
)

/**
 * https://leetcode.com/problems/largest-component-size-by-common-factor/
 * https://www.youtube.com/watch?v=GTX0kw63Tn0&list=PLLuMmzMTgVK5gFVMpryw0LkJp4l9WTtdM&index=14&t=0s
 */

type UnionFind struct {
	up   []int
	rank []int
}

func (uf *UnionFind) Find(x int) int {
	if uf.up[x] == x {
		return x
	} else {
		uf.up[x] = uf.Find(uf.up[x])
		return uf.up[x]
	}
}

func (uf *UnionFind) Union(x, y int) bool {
	reprX := uf.Find(x)
	reprY := uf.Find(y)
	if reprX == reprY {
		return false
	}
	if uf.rank[reprX] == uf.rank[reprY] {
		uf.rank[reprX]++
		uf.up[reprY] = reprX
	} else if uf.rank[reprX] > uf.rank[reprY] {
		uf.up[reprY] = reprX
	} else {
		uf.up[reprX] = reprY
	}
	return true
}

func NewUnionFind(n int) UnionFind {
	up := make([]int, n)
	for i := range up {
		up[i] = i
	}
	rank := make([]int, n)
	return UnionFind{up, rank}
}

func maxIntSlice(arr []int) int {
	var m int
	for _, v := range arr {
		m = maxInt(m, v)
	}
	return m
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func sqrtInt(i int) int {
	return int(math.Sqrt(float64(i)))
}

func largestComponentSize(A []int) int {
	n := maxIntSlice(A)
	uf := NewUnionFind(n + 1)
	for _, v := range A {
		for k := 2; k <= sqrtInt(v); k++ {
			if v%k == 0 {
				uf.Union(v, k)
				uf.Union(v, v/k)
			}
		}
	}
	count := make(map[int]int)
	for _, v := range A {
		count[uf.Find(v)]++
	}
	var ans int
	for _, v := range count {
		ans = maxInt(ans, v)
	}
	return ans
}

func main() {
	ans := largestComponentSize([]int{20, 50, 9, 63})
	println(ans)
}
