package main

func fourSumCount(A []int, B []int, C []int, D []int) int {
	sumMap := make(map[int]int)

	for i := 0; i < len(C); i++ {
		for j := 0; j < len(D); j++ {
			sum := C[i] + D[j]
			if val, exists := sumMap[sum]; exists {
				sumMap[sum] = val + 1
			} else {
				sumMap[sum] = 1
			}
		}
	}

	var res int
	for i := 0; i < len(A); i++ {
		for j := 0; j < len(D); j++ {
			sum := A[i] + B[j]
			if val, exists := sumMap[-sum]; exists {
				res += val
			}
		}
	}

	return res
}
