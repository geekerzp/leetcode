package main

import "math"

func coinChange(coins []int, amount int) int {
	if amount < 1 {
		return 0
	}
	return doCoinChange(coins, amount, make(map[int]int))
}

func doCoinChange(coins []int, rem int, count map[int]int) int {
	if rem < 0 {
		return -1
	}
	if rem == 0 {
		return 0
	}
	if count[rem-1] != 0 {
		return count[rem-1]
	}
	min := math.MaxInt64
	for _, coin := range coins {
		res := doCoinChange(coins, rem-coin, count)
		if res >= 0 && res < min {
			min = res + 1
		}
	}
	if min == math.MaxInt64 {
		count[rem-1] = -1
	} else {
		count[rem-1] = min
	}
	return count[rem-1]
}

func main() {
	coinChange([]int{1, 2, 5}, 11)
}
