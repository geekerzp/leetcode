package main

import (
	"github.com/davecgh/go-spew/spew"
)

/**
 * https://leetcode.com/problems/word-ladder-ii/
 * https://www.youtube.com/watch?v=PblfQrdWXQ4&list=PLLuMmzMTgVK423Mj1n_OaOAZZ6k5fNxyN&index=35
 */

func findLadders(beginWord string, endWord string, wordList []string) [][]string {
	dict := make(map[string]bool)
	for _, word := range wordList {
		dict[word] = true
	}
	if _, exists := dict[endWord]; !exists {
		return nil
	}
	dict[beginWord] = false
	dict[endWord] = false

	steps := map[string]int{beginWord: 1}
	parents := make(map[string][]string)
	q := []string{beginWord}

	var ans [][]string

	l := len(beginWord)
	var step int
	var found bool

	for len(q) > 0 && !found {
		step++
		for size := len(q); size > 0; size-- {
			var p string
			p, q = q[0], q[1:]
			w := p
			for i := 0; i < l; i++ {
				ch := w[i]
				for j := byte('a'); j <= 'z'; j++ {
					if j == ch {
						continue
					}
					w = replace(w, i, j)
					if w == endWord {
						parents[w] = append(parents[w], p)
						found = true
					} else {
						// Not a new word, but another transform
						// with the same number of steps
						if steps[w] > 0 && step < steps[w] {
							parents[w] = append(parents[w], p)
						}
					}

					if !dict[w] {
						continue
					}
					dict[w] = false
					q = append(q, w)
					steps[w] = steps[p] + 1
					parents[w] = append(parents[w], p)
				}
				w = replace(w, i, ch)
			}
		}
	}

	if found {
		curr := []string{endWord}
		getPaths(endWord, beginWord, parents, &curr, &ans)
	}

	return ans
}

func getPaths(
	word string,
	begingWord string,
	parents map[string][]string,
	curr *[]string,
	ans *[][]string) {
	if word == begingWord {
		path := make([]string, len(*curr))
		copy(path, *curr)
		reverse(path)
		*ans = append(*ans, path)
		return
	}

	for _, p := range parents[word] {
		*curr = append(*curr, p)
		getPaths(p, begingWord, parents, curr, ans)
		*curr = (*curr)[:len(*curr)-1]
	}
}

func replace(s string, i int, b byte) string {
	buf := []byte(s)
	buf[i] = b
	return string(buf)
}

func reverse(s []string) {
	for l, r := 0, len(s)-1; l < r; l, r = l+1, r-1 {
		s[l], s[r] = s[r], s[l]
	}
}

func main() {
	ans := findLadders("hit", "cog", []string{"hot", "dot", "dog", "lot", "log"})
	spew.Dump(ans)
}
