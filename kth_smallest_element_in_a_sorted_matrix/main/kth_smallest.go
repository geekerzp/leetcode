package main

import (
	"container/heap"
	"github.com/davecgh/go-spew/spew"
)

type Tuple struct {
	value int
	x     int
	y     int
}

type IntHeap []*Tuple

func (h IntHeap) Len() int {
	return len(h)
}

func (h IntHeap) Less(i, j int) bool {
	return h[i].value < h[j].value
}

func (h IntHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *IntHeap) Push(x interface{}) {
	*h = append(*h, x.(*Tuple))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func kthSmallest(matrix [][]int, k int) int {
	n := len(matrix)
	h := &IntHeap{}
	heap.Init(h)
	for i := 0; i < n; i++ {
		heap.Push(h, &Tuple{x: 0, y: i, value: matrix[0][i]})
	}
	for i := 0; i < k-1; i++ {
		t := heap.Pop(h).(*Tuple)
		if t.x == n-1 {
			continue
		}
		heap.Push(h, &Tuple{x: t.x + 1, y: t.y, value: matrix[t.x+1][t.y]})
	}
	return heap.Pop(h).(*Tuple).value
}

func main() {
	matrix := [][]int{{1, 5, 9}, {10, 11, 13}, {12, 13, 15}}
	k := 8
	spew.Dump(kthSmallest(matrix, k))
}
