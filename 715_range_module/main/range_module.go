package main

import "github.com/davecgh/go-spew/spew"

/**
 * https://leetcode.com/problems/range-module/
 * https://www.youtube.com/watch?v=pcpB9ux3RrQ&list=PLLuMmzMTgVK7ug02DDoQsf50OtwVDL1xd&index=27&t=1s
 */

type RangeModule struct {
	Ranges [][]int
}

func Constructor() RangeModule {
	return RangeModule{}
}

func (rm *RangeModule) AddRange(left int, right int) {
	var newRanges [][]int
	var inserted bool
	for _, r := range rm.Ranges {
		if r[0] > right {
			if !inserted {
				newRanges = append(newRanges, []int{left, right})
				inserted = true
			}
			newRanges = append(newRanges, r)
		} else if r[1] < left {
			newRanges = append(newRanges, r)
		} else {
			left = minInt(left, r[0])
			right = maxInt(right, r[1])
		}
	}
	if !inserted {
		newRanges = append(newRanges, []int{left, right})
	}
	rm.Ranges = newRanges
}

func (rm *RangeModule) QueryRange(left int, right int) bool {
	n := len(rm.Ranges)
	l := 0
	r := n - 1
	for l <= r {
		m := l + (r-l)/2
		if rm.Ranges[m][1] < left {
			l = m + 1
		} else if rm.Ranges[m][0] > right {
			r = m - 1
		} else {
			return rm.Ranges[m][0] <= left && rm.Ranges[m][1] >= right
		}
	}
	return false
}

func (rm *RangeModule) RemoveRange(left int, right int) {
	var newRanges [][]int
	for _, r := range rm.Ranges {
		if r[1] <= left || r[0] >= right {
			newRanges = append(newRanges, r)
		} else {
			if r[0] < left {
				newRanges = append(newRanges, []int{r[0], left})
			}
			if r[1] > right {
				newRanges = append(newRanges, []int{right, r[1]})
			}
		}
	}
	rm.Ranges = newRanges
}

func minInt(i, j int) int {
	if i < j {
		return i
	}
	return j
}

func maxInt(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {
	rm := Constructor()
	rm.AddRange(10, 20)
	rm.AddRange(1, 9)
	rm.AddRange(2, 12)
	spew.Dump(rm.Ranges)
}
