package main

import (
	"github.com/davecgh/go-spew/spew"
	"sort"
)

/*
 * https://leetcode.com/problems/count-of-smaller-numbers-after-self/
 */

type FenwickTree struct {
	Sums []int
}

func NewFenwickTree(n int) *FenwickTree {
	fenwickTree := new(FenwickTree)
	fenwickTree.Sums = make([]int, n+1)
	return fenwickTree
}

func (ft *FenwickTree) Update(i int, delta int) {
	for i < len(ft.Sums) {
		ft.Sums[i] += delta
		i += lowbit(i)
	}
}

func (ft *FenwickTree) Query(i int) int {
	var sum int
	for i > 0 {
		sum += ft.Sums[i]
		i -= lowbit(i)
	}
	return sum
}

func lowbit(x int) int {
	return x & (-x)
}

func countSmaller(nums []int) []int {
	// Sort the unique numbers
	sorted := make([]int, len(nums))
	copy(sorted, nums)
	sort.Ints(sorted)
	// Map the key to its rank
	ranks := make(map[int]int)
	for idx, num := range sorted {
		ranks[num] = idx + 1
	}
	var ans []int
	ft := NewFenwickTree(len(nums))
	// Scan the numbers in reserved order
	for i := len(nums) - 1; i >= 0; i-- {
		// Check haw many numbers are smaller than the current number
		ans = append([]int{ft.Query(ranks[nums[i]] - 1)}, ans...)
		// Increse the count of the rank of current number
		ft.Update(ranks[nums[i]], 1)
	}
	return ans
}

func main() {
	spew.Dump(countSmaller([]int{5, 2, 6, 1}))
}
