package main

import "github.com/davecgh/go-spew/spew"

func binaryGap(N int) int {
	var A []int
	var ans int

	for i := uint(0); i < 32; i++ {
		if ((N >> i) & 1) != 0 {
			A = append(A, int(i))
		}
	}

	for i := 0; i < len(A)-1; i++ {
		ans = max(ans, A[i+1]-A[i])
	}

	return ans
}

func max(i, j int) int {
	if i > j {
		return i
	}
	return j
}

func main() {
	spew.Dump(binaryGap(8))
}
