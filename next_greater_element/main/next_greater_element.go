package main

func nextGreaterElement(findNums []int, nums []int) []int {
	numsMap := make(map[int]int)
	var stack []int

	for _, num := range nums {
		for len(stack) > 0 && num > stack[len(stack)-1] {
			numsMap[stack[len(stack)-1]] = num
			stack = stack[:len(stack)-1]
		}

		stack = append(stack, num)
	}

	for i, _ := range findNums {
		if num, exists := numsMap[findNums[i]]; exists {
			findNums[i] = num
		} else {
			findNums[i] = -1
		}
	}

	return findNums
}
